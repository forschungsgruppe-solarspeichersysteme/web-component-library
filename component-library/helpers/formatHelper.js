class FormatHelper {
    /**
     * Method takes kebab-case input and converts it to camelCase
     * 
     * @param {string} value 
     * @returns string in camelCase format
     */
    static kebabCaseToCamelCase(value) {
        return value.replace(/-./g, x=>x[1].toUpperCase())
    }
    static pascalCaseToKebabCase(value) {
        return value.replace(/([a-z0–9])([A-Z])/g, "$1-$2").toLowerCase();
    }
    static stringToIntOnly(value){
        return value.replace(/[^\d.]/g, "");
    }
    static correctDataType(value){
        if(!isNaN(value)){
            if(Number.isInteger(value)){
                value = parseInt(value)
            }else{
                value = parseFloat(value)
            }
        }else if(value === "true" || value === "false"){
            value = value === "true"
        }
        return value;
    }
}