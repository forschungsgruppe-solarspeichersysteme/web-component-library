class SelectorHelper {
    static querySelectorAll(node,selector){
        const nodes = [...node.querySelectorAll(selector)],
            nodeIterator = document.createNodeIterator(node, Node.ELEMENT_NODE);
        let currentNode;
        while (currentNode = nodeIterator.nextNode()) {
            if(currentNode.shadowRoot) {
                nodes.push(...this.querySelectorAll(currentNode.shadowRoot,selector));
            }
        }
        return nodes;
    }
    static querySelector(node,selector){
        if(node.querySelector(selector) !== null){
            return node.querySelector(selector)
        }

        const nodeIterator = document.createNodeIterator(node, Node.ELEMENT_NODE);

        let currentNode;
        while (currentNode = nodeIterator.nextNode()) {
            if(currentNode.shadowRoot) {
                let found = this.querySelector(currentNode.shadowRoot,selector)
                if(found !== null){
                    return found
                }
            }
        }
        return null;
    }
    
}