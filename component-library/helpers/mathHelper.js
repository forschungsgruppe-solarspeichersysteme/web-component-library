class MathHelper {
    /**
     * Method rounds value var based on step
     * 
     * @param {float} value 
     * @param {float} step 
     * @returns rounded number
     */
    static roundByStep(value, step) {
      let rounded;

      let precision = this.#countDecimals(step);
      if(precision !== 0){
        let multiplier = Math.pow(10, precision || 0);
        rounded = Math.round(value * multiplier) / multiplier;
      }else{
        rounded = Math.round(value / step) * step;
      }
      
      return rounded.toFixed(precision);
    }

    static #countDecimals (value) {
        let text = value.toString()
        // verify if number 0.000005 is represented as "5e-6"
        if (text.indexOf('e-') > -1) {
          let [base, trail] = text.split('e-');
          let deg = parseInt(trail, 10);
          return deg;
        }
        // count decimals for number in representation like "0.123456"
        if (Math.floor(value) !== value) {
          return value.toString().split(".")[1].length || 0;
        }
        return 0;
    }
}

if (typeof module !== 'undefined' && module.exports) { 
  module.exports = MathHelper
}