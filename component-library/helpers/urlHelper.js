class UrlHelper {
    
    static setGetParameter(paramName, paramValue) {
        var url = new URL(window.location.href);
        url.searchParams.set(paramName, paramValue);
    
        window.history.pushState({}, '', url);
    }
    static deleteGetParameter(paramName) {
        var url = new URL(window.location.href);
        url.searchParams.delete(paramName);
    
        window.history.pushState({}, '', url);
    }
    static getGetParameter(paramName) {
        var url = window.location.search.substring(1); // get the query string without the initial "?"
        var params = url.split('&'); // split the parameters
    
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('='); // split key/value pairs
    
            if (param[0] === paramName) {
                return param[1]; // return the value if the key matches
            }
        }
    
        return null; // return null if the key is not found
    }
}