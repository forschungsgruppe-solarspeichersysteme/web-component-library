/**
 * Attributes: 
 * fill - (float) precentage of the infill of the animation
 * src - (string) src-path of the image
 * alt - (string) alt text for image
 * mask-src - (string) src-path of the mask-image
 *
 * Attribute change callback:
 * fill
 *
 * How to use:
 * <fill-result-animation fill="50" animation="outlet"></fill-result-animation>
 */

class FillResultAnimation extends BaseElement {
    /**
     * DOM elements
     */
    underlay
 
    /**
     * Attributes
     */
    fill
    src
    maskSrc
    alt

    static get observedAttributes() {
        return ['fill'];
    }

    static get observedAttributesFunction() {
        return {
            'fill' : function (newValue, that){
                that.fill = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.fill = this.getAttribute("fill");
        this.animation = this.getAttribute("animation");
        this.src = this.getAttribute("src");
        this.alt = this.getAttribute("alt");
        this.maskSrc = this.getAttribute("mask-src");
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/animations/fillResultAnimation.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

         const underlay = document.createElement("div");
         underlay.className="underlay";
         underlay.style.setProperty('mask-image', `url(${"../../../"+this.maskSrc})`);
         underlay.style.setProperty('-webkit-mask-image', `url(${"../../../"+this.maskSrc})`);
         outerWrapper.appendChild(underlay);

         const svg = document.createElement("img");
         svg.src=this.src;
         svg.alt=this.alt;
         outerWrapper.appendChild(svg);

         switch(this.animation){
            case "piggy-bank":
                svg.src="img/results/piggybank.svg"
                svg.alt="Sparschwein"
                break;
            case "outlet":
                svg.src="img/results/outlet.svg"
                svg.alt="Steckdose"
                break;
            case "radiator":
                svg.src="img/results/radiator.svg"
                svg.alt="Heizkörper"
                break;
            case "car":
                svg.src="img/results/car.svg"
                svg.alt="Auto"
                break;
         }
    }

    setEvents(){
        this.underlay = this.shadowRoot.querySelector('.underlay');
        this.updateElements();

    }

    updateElements(){
        this.underlay.style.setProperty('--top', 100-this.fill + "%");
    }

}
customElements.define('fill-result-animation', FillResultAnimation)
