/**
 * Attributes: 
 * data - (json-string) data attribute for the animation
 *
 * Attribute change callback:
 * data
 *
 * How to use:
 * <logo-background-animation data='{"src": "/assets/img/background-co2.svg", "alt": "Grafik mit Icons für Auto, Flugzeug und Schiff", "text": "18,4 t", "unit" : "CO₂"}'></logo-background-animation>
 */

class LogoBackgroundAnimation extends BaseElement {
    /**
     * DOM elements
     */

 
    /**
     * Attributes
     */
    /**
     * data = {
     *  src: (string) src-path of the image	
     *  alt: (string) alt text for image
     *  text: (string) text in the middle of the circle
     *  unit: (string) unit of the value
     *  size: (string) size of the circle in em	
     * }
     */
    data;

    static get observedAttributes() {
        return ['data'];
    }

    static get observedAttributesFunction() {
        return {
            'data' : function (newValue, that){
                that.data = JSON.parse(newValue);
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.data = JSON.parse(this.getAttribute("data"));
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/animations/logoBackgroundAnimation.css")
    
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        const img = document.createElement("img");
        img.src = this.data.src;
        img.alt = this.data.alt;
        outerWrapper.appendChild(img);

        const textWrapper = document.createElement("div");
        textWrapper.className="text-wrapper";
        outerWrapper.appendChild(textWrapper);

        const textSpan = document.createElement("span");
        textSpan.className="text";
        textWrapper.appendChild(textSpan);

        const unitSpan = document.createElement("span");
        unitSpan.className="unit";
        unitSpan.innerText = this.data.unit;
        textWrapper.appendChild(unitSpan);    
    }

    setEvents(){
        this.textSpan = this.shadowRoot.querySelector("span.text");
        this.updateElements();
    }

    updateElements(){
        this.textSpan.innerText = this.data.text;
    }

}
customElements.define('logo-background-animation', LogoBackgroundAnimation);
