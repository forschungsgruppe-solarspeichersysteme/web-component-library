/**
 * Attributes: 
 * value - (int) current value on the compass (Osten,  Südosten, Süden, Südwesten, Westen)
 * additional-animation-value - (bool) if set to "true", the roof has double sided solar panels
 * 
 * Attribute change callback:
 * value
 * additional-animation-value
 *
 * How to use:
 * <compass-solar-roof value="Süden"></compass-solar-roof>
 */

class CompassSolarRoof extends BaseElement {
    /**
     * DOM elements
     */
    roofReference
    rightRoof

    /**
     * Attributes
     */
    value
    additionalAnimationValue

    static get observedAttributes() {
        return ['value', 'additional-animation-value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            },
            'additional-animation-value' : function (newValue, that){
                that.additionalAnimationValue = newValue == "true";
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.additionalAnimationValue = this.getAttribute("additional-animation-value")  == "true";
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/animations/compassSolarRoof.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);


         const frame = document.createElement("div");
         frame.className="frame";
         frame.setAttribute("aria-hidden", "true");
         outerWrapper.appendChild(frame);

         const n = document.createElement("span");
         n.innerHTML="N"
         n.className="n pole";
         frame.appendChild(n);

         const e = document.createElement("span");
         e.innerHTML="O"
         e.className="e pole";
         frame.appendChild(e);

         const s = document.createElement("span");
         s.innerHTML="S"
         s.className="s pole";
         frame.appendChild(s);

         const w = document.createElement("span");
         w.innerHTML="W"
         w.className="w pole";
         frame.appendChild(w);

         const roofReference = document.createElement("div");
         roofReference.className="roof-reference";
         frame.appendChild(roofReference);

         const left = document.createElement("div");
         left.className="roof-site left";
         roofReference.appendChild(left);

         for(let i = 0; i < 2; i++){
            const panel = document.createElement("div");
            panel.className="panel";
            left.appendChild(panel);
         }

         const right = document.createElement("div");
         right.className="roof-site right";
         roofReference.appendChild(right);

         for(let i = 0; i < 2; i++){
            const panel = document.createElement("div");
            panel.className="panel";
            right.appendChild(panel);
         }
    }

    setEvents(){
        this.roofReference = this.shadowRoot.querySelector('.roof-reference');
        this.rightRoof = this.shadowRoot.querySelector('.roof-site.right');
        this.updateElements()
    }
    updateElements(){
        let deg;
        switch(this.value){
            case "Osten":
                deg = -90
                break;
            case "Südosten":
                deg = -45
                break;
            case "Süden":
                deg = 0
                break;
            case "Südwesten":
                deg = 45
                break;
            case "Westen":
                deg = 90
                break;
        }

        this.roofReference.style.rotate = deg + "deg"

        if(this.additionalAnimationValue){
            this.rightRoof.classList.remove("invisible-solar");
        }else{
            this.rightRoof.classList.add("invisible-solar");

        }
    }

    calcTop(deg){
        return "-" + 3 * Math.sin(this.toRadians(deg)) + "em";
    }
    
    toRadians (angle) {
        return angle * (Math.PI / 180);
    }
}
customElements.define('compass-solar-roof', CompassSolarRoof)
