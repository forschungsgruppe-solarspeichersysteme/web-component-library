/**
 * Attributes: 
 * value - tilt degree of the solar panels
 *
 * Attribute change callback:
 * value
 *
 * How to use:
 * <flat-solar-roof value="50"></flat-solar-roof>
 */

class FlatSolarRoof extends BaseElement {
    /**
     * DOM elements
     */
    cube
    panels

    /**
     * Attributes
     */
    value

    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/animations/flatSolarRoof.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

         const frame = document.createElement("div");
         frame.className="frame";
         outerWrapper.appendChild(frame);

        const cube = document.createElement("div");
        cube.className="cube";
        frame.appendChild(cube);
        
        const front = document.createElement("div");
        front.className="front";
        cube.appendChild(front);

        const right = document.createElement("div");
        right.className="right";
        cube.appendChild(right);

        const left = document.createElement("div");
        left.className="left";
        cube.appendChild(left);

        const top = document.createElement("div");
        top.className="top";
        cube.appendChild(top);

        const back = document.createElement("div");
        back.className="back";
        cube.appendChild(back);

        // const bottom = document.createElement("div");
        // bottom.className="bottom";
        // cube.appendChild(bottom);

         for(let i = 0; i < 2; i++){
            const panel = document.createElement("div");
            panel.className="panel";
            top.appendChild(panel);

            const antiPanel = document.createElement("div");
            antiPanel.className="anti-panel";
            panel.appendChild(antiPanel);
         }

    }

    setEvents(){
        this.cube = this.shadowRoot.querySelector('.cube');
        this.panels = this.shadowRoot.querySelectorAll('.panel');
        this.antiPanels = this.shadowRoot.querySelectorAll('.anti-panel');

        this.updateElements()
    }
    updateElements(){
        let deg = parseInt(this.value)
        let depth=6;
        let a = 2*2* Math.cos(deg * Math.PI / 180);

        this.panels.forEach(panel => {
            panel.style.transform = ("rotateX(-" + deg + "deg)");
            panel.style.top = (depth-a)/2 + "em";
        });
        this.antiPanels.forEach(panel => {
            panel.style.transform = ("rotateX(" + (180 + 2 *deg) + "deg)");
        });


    }
}
customElements.define('flat-solar-roof', FlatSolarRoof)
