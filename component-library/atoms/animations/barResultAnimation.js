/**
 * Attributes: 
 * data - (json-string) data attribute for the animation
 *
 * Attribute change callback:
 * data
 *
 * How to use:
 * <bar-result-animation data='{"firstValue": "27.50", "secondValue": "37.27", "firstText": "Mieterstrompreis", "secondText": "max. Preis", "unit" : "ct/kWh", "size": "2em"}'></bar-result-animation>
 */

class BarResultAnimation extends BaseElement {
    /**
     * DOM elements
     */
    firstBar;
    secondBar;

    firstValueSpan;
    secondValueSpan;

    firstTextSpan;
    secondTextSpan;
 
    /**
     * Attributes
     */
    /**
     * data = {
     *  firstValue: (float) value of first bar
     *  secondValue: (float) value of second bar
     *  firstText: (string) text of first bar
     *  secondText: (string) text of second bar
     *  unit: (string) unit of the value
     *  size: (string) size of the circle in em	
     * }
     */
    data;

    static get observedAttributes() {
        return ['data'];
    }

    static get observedAttributesFunction() {
        return {
            'data' : function (newValue, that){
                that.data = JSON.parse(newValue);
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.data = JSON.parse(this.getAttribute("data"));
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/animations/barResultAnimation.css")
    
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        // if(this.data.size){
        //     outerWrapper.style.fontSize = this.data.size;
        // }
        this.template.content.appendChild(outerWrapper);

        const firstWrapper = document.createElement("div");
        firstWrapper.className="first-wrapper";
        outerWrapper.appendChild(firstWrapper);

        const firstBar = document.createElement("div");
        firstBar.className="bar";
        firstWrapper.appendChild(firstBar);

        const firstValueSpan = document.createElement("span");
        firstValueSpan.className="value";
        firstWrapper.appendChild(firstValueSpan);

        const firstTextSpan = document.createElement("span");
        firstTextSpan.className="text";
        firstWrapper.appendChild(firstTextSpan);

        const secondWrapper = document.createElement("div");
        secondWrapper.className="second-wrapper";
        outerWrapper.appendChild(secondWrapper);

        const secondBar = document.createElement("div");
        secondBar.className="bar";
        secondWrapper.appendChild(secondBar);

        const secondValueSpan = document.createElement("span");
        secondValueSpan.className="value";
        secondWrapper.appendChild(secondValueSpan);

        const secondTextSpan = document.createElement("span");
        secondTextSpan.className="text";
        secondWrapper.appendChild(secondTextSpan);    
    }

    setEvents(){
        this.firstBar = this.shadowRoot.querySelector(".first-wrapper .bar");
        this.secondBar = this.shadowRoot.querySelector(".second-wrapper .bar");

        this.firstValueSpan = this.shadowRoot.querySelector(".first-wrapper .value");
        this.secondValueSpan = this.shadowRoot.querySelector(".second-wrapper .value");

        this.firstTextSpan = this.shadowRoot.querySelector(".first-wrapper .text");
        this.secondTextSpan = this.shadowRoot.querySelector(".second-wrapper .text");

        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');

        this.updateElements();
    }

    updateElements(){
        this.secondBar.style.backgroundColor = "#d0d0d0";

        if(this.data.firstValue > this.data.secondValue){
            this.firstBar.style.height = "4.5em";
            // this.secondBar.style.backgroundColor = "#d0d0d0";
            // this.firstBar.style.backgroundColor = "";
            this.secondBar.style.height = (this.data.secondValue / this.data.firstValue) * 4.5 + "em";
        }else{
            this.firstBar.style.height = (this.data.firstValue / this.data.secondValue) * 4.5 + "em";
            // this.firstBar.style.backgroundColor = "#d0d0d0";
            // this.secondBar.style.backgroundColor = "";
            this.secondBar.style.height = "4.5em";
        }
        this.firstTextSpan.innerText = this.data.firstText;
        this.secondTextSpan.innerText = this.data.secondText;

        this.firstValueSpan.innerText = this.data.firstValue + " " + this.data.unit;
        this.secondValueSpan.innerText = this.data.secondValue + " " + this.data.unit;
    }

}
customElements.define('bar-result-animation', BarResultAnimation);
