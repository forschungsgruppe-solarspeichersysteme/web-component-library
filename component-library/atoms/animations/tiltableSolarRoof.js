/**
 * Attributes: 
 * value - tilt degree of the roof
 *
 * Attribute change callback:
 * value
 *
 * How to use:
 * <tiltable-solar-roof value="50"></tiltable-solar-roof>
 */

class TiltableSolarRoof extends BaseElement {
    /**
     * DOM elements
     */
    roofReference
    leftRoof
    rightRoof

    /**
     * Attributes
     */
    value

    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/animations/tiltableSolarRoof.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

         const frame = document.createElement("div");
         frame.className="frame";
         outerWrapper.appendChild(frame);

         const roofReference = document.createElement("div");
         roofReference.className="roof-reference";
         frame.appendChild(roofReference);

         const left = document.createElement("div");
         left.className="roof-site left";
         roofReference.appendChild(left);

         for(let i = 0; i < 2; i++){
            const panel = document.createElement("div");
            panel.className="panel";
            left.appendChild(panel);
         }

         const right = document.createElement("div");
         right.className="roof-site right";
         roofReference.appendChild(right);
    }

    setEvents(){
        this.roofReference = this.shadowRoot.querySelector('.roof-reference');
        this.leftRoof = this.shadowRoot.querySelector('.left');
        this.rightRoof = this.shadowRoot.querySelector('.right');
        this.updateElements()
    }
    updateElements(){
        let deg = parseInt(this.value)
        this.leftRoof.style.transform = ("rotateX(-" + deg + "deg)");
        this.rightRoof.style.transform = ("rotateX(" + (180 + deg) + "deg)");
        this.roofReference.style.top = this.calcTop(deg)
    }

    calcTop(deg){
        return "-" + 3 * Math.sin(this.toRadians(deg)) + "em";
    }
    
    toRadians (angle) {
        return angle * (Math.PI / 180);
    }
}
customElements.define('tiltable-solar-roof', TiltableSolarRoof)
