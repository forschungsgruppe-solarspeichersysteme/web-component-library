/**
 * Attributes: 
 * value - (int) current value on the compass (Osten,  Südosten, Süden, Südwesten, Westen)
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <compass-facade-solar value="Südosten"></compass-facade-solar>
 */

class compassFacadeSolar extends BaseElement {
    /**
     * DOM elements
     */
    roofReference
    rightRoof

    /**
     * Attributes
     */
    value

    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/animations/compassFacadeSolar.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);


         const frame = document.createElement("div");
         frame.className="frame";
         frame.setAttribute("aria-hidden", "true");
         outerWrapper.appendChild(frame);

         const n = document.createElement("span");
         n.innerHTML="N"
         n.className="n pole";
         frame.appendChild(n);

         const e = document.createElement("span");
         e.innerHTML="O"
         e.className="e pole";
         frame.appendChild(e);

         const s = document.createElement("span");
         s.innerHTML="S"
         s.className="s pole";
         frame.appendChild(s);

         const w = document.createElement("span");
         w.innerHTML="W"
         w.className="w pole";
         frame.appendChild(w);

         const roofReference = document.createElement("div");
         roofReference.className="roof-reference";
         frame.appendChild(roofReference);

         const left = document.createElement("div");
         left.className="roof-site left";
         roofReference.appendChild(left);

         for(let i = 0; i < 2; i++){
            const panel = document.createElement("div");
            panel.className="panel";
            left.appendChild(panel);
         }

         const right = document.createElement("div");
         right.className="roof-site right";
         roofReference.appendChild(right);
    }

    setEvents(){
        this.roofReference = this.shadowRoot.querySelector('.roof-reference');
        this.rightRoof = this.shadowRoot.querySelector('.roof-site.right');
        this.updateElements()
    }
    updateElements(){
        let deg;
        switch(this.value){
            case "Osten":
                deg = -90
                break;
            case "Südosten":
                deg = -45
                break;
            case "Süden":
                deg = 0
                break;
            case "Südwesten":
                deg = 45
                break;
            case "Westen":
                deg = 90
                break;
        }

        this.roofReference.style.rotate = deg + "deg"

       
    }

    calcTop(deg){
        return "-" + 3 * Math.sin(this.toRadians(deg)) + "em";
    }
    
    toRadians (angle) {
        return angle * (Math.PI / 180);
    }
}
customElements.define('compass-facade-solar', compassFacadeSolar)
