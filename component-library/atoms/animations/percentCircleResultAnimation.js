/**
 * Attributes: 
 * data - (json-string) data attribute for the animation
 *
 * Attribute change callback:
 * data
 *
 * How to use:
 * <percent-circle-result-animation data='{"fill": "30", "secondFill": "20", "value": "50%", "size": "2em"}'></percent-circle-result-animation>
 */

class PercentCircleResultAnimation extends BaseElement {
    /**
     * DOM elements
     */
    fillCircle;
    secondFillCircle;
    span;
 
    /**
     * Attributes
     */
    /**
     * data = {
     *  fill: (float) precentage of the rotary infill of the animation
     *  secondFill (optional): (float) precentage of the second rotary infill of the animation
     *  value: (string) value of the text in the center
     *  size: (string) size of the circle in em
     * }
     */
    data;

    static get observedAttributes() {
        return ['data'];
    }

    static get observedAttributesFunction() {
        return {
            'data' : function (newValue, that){
                that.data = JSON.parse(newValue);
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.data = JSON.parse(this.getAttribute("data"));
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/animations/percentCircleResultAnimation.css")
    
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        // if(this.data.size){
        //     outerWrapper.style.fontSize = this.data.size;
        // }
        this.template.content.appendChild(outerWrapper);

        const animation = document.createElementNS("http://www.w3.org/2000/svg","svg");
        animation.setAttribute("class", "progress-ring");
        outerWrapper.appendChild(animation);

        const circle = document.createElementNS("http://www.w3.org/2000/svg","circle");
        //  circle.className="progress-ring_circle";
        circle.setAttribute("class", "background-circle");
        circle.setAttribute("stroke", "#d0d0d0");
        circle.setAttribute("stroke-width", "1em");
        circle.setAttribute("fill", "transparent");
        circle.setAttribute("r", "3.25em");
        circle.setAttribute("cx", "4em");
        circle.setAttribute("cy", "4em");
        animation.appendChild(circle);

        if(this.data.secondFill){
            const secondFill = document.createElementNS("http://www.w3.org/2000/svg","circle");
            //  circle.className="progress-ring_circle";
            secondFill.setAttribute("class", "progress-ring_circle second-fill");
            secondFill.setAttribute("stroke", "#ebc500");
            secondFill.setAttribute("stroke-width", "1em");
            secondFill.setAttribute("fill", "transparent");
            secondFill.setAttribute("r", "3.25em");
            secondFill.setAttribute("cx", "4em");
            secondFill.setAttribute("cy", "4em");
            animation.appendChild(secondFill);
        }

        const fill = document.createElementNS("http://www.w3.org/2000/svg","circle");
        fill.setAttribute("class", "progress-ring_circle fill");
        fill.setAttribute("stroke", "#76b921");
        fill.setAttribute("stroke-width", "1em");
        fill.setAttribute("fill", "transparent");
        fill.setAttribute("r", "3.25em");
        fill.setAttribute("cx", "4em");
        fill.setAttribute("cy", "4em");
        animation.appendChild(fill);

        const text = document.createElement("span");
        text.className="text";
        outerWrapper.appendChild(text);       
    }

    setEvents(){
        this.fillCircle = this.shadowRoot.querySelector('.fill');
        this.secondFillCircle = this.shadowRoot.querySelector('.second-fill');
        this.span = this.shadowRoot.querySelector('.text');
        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.updateElements();
    }

    updateElements(){
        // if(this.data.size){
        //     this.outerWrapper.style.fontSize = this.data.size;
        // }else{
        //     this.outerWrapper.style.fontSize = "";
        // }
        this.span.innerText = this.data.value;
        var radius = this.fillCircle.r.baseVal.value;
        var circumference = radius * 2 * Math.PI;
        var offset = circumference - parseFloat(this.data.fill) / 100 * circumference;
        // console.log(circumference, offset, this.data);
        this.fillCircle.style.strokeDasharray = `${circumference} ${circumference}`;
        this.fillCircle.style.strokeDashoffset = offset;

        // if(!this.data.secondFill) return;

        radius = this.secondFillCircle.r.baseVal.value;
        circumference = radius * 2 * Math.PI;
        offset = circumference - (parseFloat(this.data.fill) + parseFloat(this.data.secondFill)) / 100 * circumference;
        this.secondFillCircle.style.strokeDasharray = `${circumference} ${circumference}`;
        this.secondFillCircle.style.strokeDashoffset = offset;
    }

}
customElements.define('percent-circle-result-animation', PercentCircleResultAnimation);
