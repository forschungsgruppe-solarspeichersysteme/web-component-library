/**
 * This is the Base Element which should be the high
 */

class BaseElement extends HTMLElement {
    template;

    connectedCallback(){
        this.attachShadow({mode: 'open'});
        this.template = document.createElement("template")      
        this.setAttributes();

        // this.addOwnStylesheet();
        this.build();
        this.shadowRoot.replaceChildren(this.template.content.cloneNode(true));
        this.setEvents();
    }

    /**
     * Implementation required
     */
    build() {
        throw new Error('build() is not implemented!');
    }

    /**
     * Implementation required
     */
    setAttributes() {
        throw new Error('setAttributes() is not implemented!');
    }

    /**
     * Implementation optional
     */
    setEvents() {
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        // removed && oldValue !== newValue && eval("this." + FormatHelper.kebabCaseToCamelCase(propertyName) + " != newValue") to change also when value is same
        if(oldValue !== null){
            this.constructor.observedAttributesFunction[propertyName](newValue, this)
        }else if(propertyName === "tooltip"){
            this.constructor.observedAttributesFunction[propertyName](newValue, this)
        }
    }
    addOwnStylesheet(path){
        this.addStylesheet(Initializer.mainPath + path)
    }
    addOwnStylesheetTest(){
        // Get stack string
        const stack = new Error().stack.split('at');
        // console.log(import.meta.url)
        console.log(stack)

        // Get the last entry
        const scriptPath = stack[stack.length - 1].trim();

        // The component will share the path up to the last slash
        const componentPath = scriptPath.substring(0, scriptPath.lastIndexOf('/'));
        console.log(scriptPath)
        const re = new RegExp("\/\w*\/\w*\/\w*(?=\.js)");

        const regexp = /\w*\/\w*\/\w*(?=\.js)/g;
        const matches = scriptPath.matchAll(regexp);
            
        for (const match of matches) {
            this.addStylesheet(match+".css")
            console.log(match+".css");
        }
    }

    /**
     * Adds a new HTMLElement before the specified className
     * 
     * @param {Sring} className 
     * @param {HTMLElement} element 
     */
    insertBeforeClass(className, element){
        let ref = this.template.content.querySelectorAll("."+className)[0]
        if(ref == undefined){
            console.error("Class could not be found")
            return;
        }
        ref.parentNode.insertBefore(element, ref)
    }

    /**
     * Adds a new HTMLElement after the specified className
     * 
     * @param {Sring} className 
     * @param {HTMLElement} element 
     */
    insertAfterClass(className, element){
        let ref = this.template.content.querySelectorAll("."+className)[0]
        if(ref == undefined){
            console.error("Class could not be found")
            return;
        }
        ref.parentNode.insertBefore(element, ref.nextSibling)
    }

    /**
     * Adds a new stylesheet element to the template 
     * 
     * @param {String} href 
     */
    addStylesheet(href){
        //Create new stylesheet element
        const newStylesheet = document.createElement("link");
        newStylesheet.rel="stylesheet";
        newStylesheet.href=href;

        //Get all stylesheet elements
        let stylesheets = this.template.content.querySelectorAll('link[rel="stylesheet"]');

        if(stylesheets.length == 0){
            //Add first stylesheet element to front position
            this.template.content.insertBefore(newStylesheet, this.template.content.children[0])
            // console.log(this.template)
        }else{
            //Pick the last stylesheet element and add the new one after it
            let lastStylesheet = stylesheets[stylesheets.length - 1];
            lastStylesheet.parentNode.insertBefore(newStylesheet, lastStylesheet.nextSibling)
        }

    }

    createObserver(element, attributeName){
        let that = this;
        //observe elements for value changes
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                let camelCaseAttributeName = FormatHelper.kebabCaseToCamelCase(attributeName)
                if (mutation.type === "attributes" && mutation.attributeName === attributeName && mutation.target.getAttribute(attributeName) != eval("that." + camelCaseAttributeName)) {
                    that.setAttribute(attributeName, mutation.target.getAttribute(attributeName));
                }
            });
        });
    
        [element].forEach((node) => {
            observer.observe(node, {
                attributes: true
            });
        })
    }
}

