/**
 * Attributes: 
 * min - (int) Minimal slider value
 * max - (int) Maximum slider value
 * step - (int) discrete step distance between min and max (pay attention that step makes sense)
 * value - (int) current value on the slider
 * drag - (bool) is slider currently dragged
 * tick-step (optional) - (int) discrete tick-step distance between min and max
 * number-nth-tick (optional) - (int) number on every n-th tick on the slider
 * horizontal (optional) - (bool) sets the slider horizontal
 * 
 * Attribute change callback:
 * value
 * horizontal
 *
 * How to use:
 * <vertical-range-slider min=0 max=1000 step=100 value=300 drag="false"></vertical-range-slider>
 */

class VerticalRangeSlider extends BaseElement {
    /**
     * DOM elements
     */
    thumb
    infillBar
    bar
    outerWrapper

    /**
     * Attributes
     */
    min
    max
    step
    value
    tickStep
    numberNthTick
    drag
    horizontal

    static get observedAttributes() {
        return ['value', 'horizontal'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.min = parseFloat(this.getAttribute("min"))
        this.max = parseFloat(this.getAttribute("max"))
        this.step = parseFloat(this.getAttribute("step"))
        this.value = parseFloat(this.getAttribute("value"))
        this.tickStep = parseInt(this.getAttribute("tick-step"))
        this.numberNthTick = parseInt(this.getAttribute("number-nth-tick"))
        this.drag = this.getAttribute("drag") == "true"; 
        this.horizontal = this.getAttribute("horizontal") == "true";
    }
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/sliders/verticalRangeSlider.css")

         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         if(this.horizontal){
            outerWrapper.classList.add("horizontal")
        }else{
            outerWrapper.classList.remove("horizontal")
        }
         outerWrapper.setAttribute('draggable', false);
         this.template.content.appendChild(outerWrapper);
 
             const wrapper = document.createElement("div");
             wrapper.className="slider-wrapper";
             wrapper.setAttribute('draggable', false);
             outerWrapper.appendChild(wrapper);
 
                const sliderInfill = document.createElement("div");
                sliderInfill.className="slider-infill";
                sliderInfill.setAttribute('draggable', false);
                wrapper.appendChild(sliderInfill);

                const sliderThumb = document.createElement("div");
                sliderThumb.className="slider-thumb";
                sliderThumb.setAttribute('draggable', false);
                wrapper.appendChild(sliderThumb);

            if(this.tickStep){
                const tickWrapper = document.createElement("div");
                tickWrapper.className="tick-wrapper";
                outerWrapper.appendChild(tickWrapper);
                for (let i = 0; i <= (this.max - this.min) / this.tickStep; i++) {
                    const tick = document.createElement("div");
                    tick.className="tick";
                    tickWrapper.appendChild(tick);
                    if(i%this.numberNthTick == 0){
                        tick.classList.add("thick");   
                    }
                }
            }
            if(this.numberNthTick){
                const tickNumberWrapper = document.createElement("div");
                tickNumberWrapper.className="tick-number-wrapper";
                outerWrapper.appendChild(tickNumberWrapper);
                for (let i = 0; i <= (this.max - this.min) / this.tickStep; i++) {
                    const tickNumber = document.createElement("span");
                    tickNumber.className="tick-number";
                    if(i%this.numberNthTick == 0){
                        const tickNumberSpan = document.createElement("span");
                        tickNumberSpan.innerText = this.tickStep * i + this.min;   
                        tickNumber.appendChild(tickNumberSpan);     
                    }
                    tickNumberWrapper.appendChild(tickNumber);
                }
            }
    }

    setEvents(){
        //adding element references
        let that = this;
        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.thumb = this.shadowRoot.querySelector('.slider-thumb');
        this.infillBar = this.shadowRoot.querySelector('.slider-infill');
        this.bar = this.shadowRoot.querySelector('.slider-wrapper');
        
        //Event listeners for thumb movement
        let bMouseDownThumb = false;
        let bTouchDownThumb = false;

          //Event listeners for mouse drag
          this.thumb.addEventListener('pointerdown', (e) => {
            bMouseDownThumb = true;
            that.setAttribute("drag", true)
            this.thumb.setPointerCapture(e.pointerId);
        });

        this.thumb.addEventListener('pointerup', (e) => {
            bMouseDownThumb = false;
            that.setAttribute("drag", false)
            this.thumb.releasePointerCapture(e.pointerId);
        });

        //Event listeners for mouse drag
        this.thumb.addEventListener('mousedown', (e) => {
            bMouseDownThumb = true;
            that.setAttribute("drag", true)
            this.thumb.setPointerCapture(1);
        });

        this.thumb.addEventListener('mouseup', (e) => {
            bMouseDownThumb = false;
            that.setAttribute("drag", false)
            this.thumb.releasePointerCapture(1);
        });

        this.thumb.addEventListener('touchstart', (e) => {
            e.preventDefault(); 
            bMouseDownThumb = true;
            that.setAttribute("drag", true)
            // this.thumb.setPointerCapture(e.pointerId);
        });
        
        this.thumb.addEventListener('touchend', (e) => {
            e.preventDefault(); 
            bMouseDownThumb = false;
            that.setAttribute("drag", false)

            // this.thumb.releasePointerCapture(e.pointerId);
        });

        this.addEventListener("mousemove",function(e){
            // console.log("mouemove")
            if(bMouseDownThumb){
                var bounds = e.target.getBoundingClientRect();
                if(this.horizontal){
                    var x = e.clientX - bounds.left - FormatHelper.stringToIntOnly(window.getComputedStyle(that.outerWrapper).marginLeft);
                    that.moveElements(x);
                }else{
                    var y = bounds.bottom - e.clientY - FormatHelper.stringToIntOnly(window.getComputedStyle(that.outerWrapper).marginBottom);
                    that.moveElements(y);
                }
            }
        });

        // //Event listeners for touch drag
        // this.thumb.addEventListener('touchstart', (e) => {
        //     bTouchDownThumb = true;
        //     this.thumb.setPointerCapture(1);
        // });
    
        // this.thumb.addEventListener('touchend', (e) => {
        //     bTouchDownThumb = false;
        //     this.thumb.releasePointerCapture(1);
        // });

        this.addEventListener("touchmove",function(e){

            if(bMouseDownThumb){
                var bounds = e.target.getBoundingClientRect();
                if(this.horizontal){
                    var x = e.touches[0].clientX - bounds.left - FormatHelper.stringToIntOnly(window.getComputedStyle(that.outerWrapper).marginLeft);
                    that.moveElements(x);
                }else{ 
                    var y = bounds.bottom - e.touches[0].clientY - FormatHelper.stringToIntOnly(window.getComputedStyle(that.outerWrapper).marginBottom);
                    that.moveElements(y);
                }
            }
        });


        //Event listener for click on bar
        this.bar.onpointerdown = (e) => {
            if(e.target == this.bar || e.target == this.infillBar){
                var bounds = e.target.getBoundingClientRect();

                if(this.horizontal){
                    var x = e.clientX - bounds.left;
                    that.moveElements(x);
                }else{
                    var y = bounds.bottom - e.clientY;
                    that.moveElements(y);
                }
                bMouseDownThumb = true;
                that.setAttribute("drag", true)
                this.bar.setPointerCapture(e.pointerId);
            }
        };
        this.bar.onpointerup  = (e) => {
            bMouseDownThumb = false;
            that.setAttribute("drag", false)
            this.bar.releasePointerCapture(e.pointerId);
        };
        this.bar.addEventListener('touchend', (e) => {
            // console.log("bar touchend")
            e.preventDefault(); 
            bMouseDownThumb = false;
            that.setAttribute("drag", false)

            // this.thumb.releasePointerCapture(e.pointerId);
        });

        that.moveElementsOnValue()
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        if(propertyName === "value" && oldValue !== null && this.value != newValue){
            this.value = newValue;
            this.moveElementsOnValue();
        }

        if(propertyName === "horizontal" && oldValue !== null && this.horizontal != (newValue == "true")){
            this.horizontal = newValue == "true";
            if(this.horizontal){
                this.outerWrapper.classList.add("horizontal")
            }else{
                this.outerWrapper.classList.remove("horizontal")
            }
            this.moveElementsOnValue();

        }
    }

    /**
     * Move slider element because of user interaction
     * 
     * @param {Float} clientX 
     */
    moveElements(offset, bPercent = false){
        if(!bPercent){
            if(this.horizontal){
                if(this.bar.clientWidth == 0)
                    return;
                offset = (offset / this.bar.clientWidth) * 100;
            }else{
                if(this.bar.clientHeight == 0)
                    return;
                offset = (offset / this.bar.clientHeight) * 100;
            }
        }

        // -1 is set because if the width is a float the browser rounds the value to the nearest int in js, but in the browser it is still a float. 
        // This results in a possible infill longer tzhan the bar itself. 
        if(this.horizontal){
            let leftPercentageOffset = Math.max(Math.min(offset, 100), 0);
            this.infillBar.style.width = leftPercentageOffset + "%";
            this.infillBar.style.height = '';
            this.thumb.style.left = leftPercentageOffset + "%";
            this.thumb.style.bottom = '';
            this.updateValue(leftPercentageOffset)
        }else{
            let bottomPercentageOffset = Math.max(Math.min(offset, 100), 0);
            this.infillBar.style.width = '';
            this.infillBar.style.height = bottomPercentageOffset + "%";
            this.thumb.style.bottom = bottomPercentageOffset + "%";
            this.thumb.style.left = '';
            this.updateValue(bottomPercentageOffset)
        }
    }

    /**
     * Move slider element because attribute value changed or resize of window
     */
     moveElementsOnValue(){
        this.moveElements(this.getPercentPerStep() * ((this.value - this.min) / this.step), true)
    }

    /**
     * Checks if the the value needs to change based on user interaction. If so it sets the value attribute
     * 
     * @param {FLoat} bottomPercentageOffset 
     */
    updateValue(bottomPercentageOffset){
        let currentStep = Math.round(bottomPercentageOffset / this.getPercentPerStep());
        let newValue = this.min + this.step * currentStep;

        //round it
        newValue = MathHelper.roundByStep(newValue, this.step)

        if(this.value != newValue){
            this.value = newValue;
            this.setAttribute("value", newValue)
        }
    }

    /**
     * Calculates the amount of pixels that the bar can move before the value increses or decreses. 
     * 
     * @returns {Float}
     */
    getPercentPerStep(){
        // console.log(this.bar.clientWidth)
        let totalSteps = (this.max - this.min) / this.step;
        return 100 / totalSteps;
    }
}
customElements.define('vertical-range-slider', VerticalRangeSlider)
