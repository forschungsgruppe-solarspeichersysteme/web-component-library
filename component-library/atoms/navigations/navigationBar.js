/**
 * Attributes: 
 * steps - (int) number of steps shoiwn in the nav bar
 * current-step - (int) current step on the nav bar
 * 
 * Attribute change callback:
 * current-step
 *
 * How to use:
 * <navigation-bar steps="6" current-step="1"></navigation-bar>
 */

class NavigationBar extends BaseElement {
    /**
     * DOM elements
     */
    bubbles
    lines

    /**
     * Attributes
     */
    steps
    currentStep

    static get observedAttributes() {
        return ['current-step'];
    }

    static get observedAttributesFunction() {
        return {
            'current-step' : function (newValue, that){
                that.currentStep = parseInt(newValue);
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.steps = this.getAttribute("steps");
        this.currentStep = this.getAttribute("current-step");
    }

    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/navigations/navigationBar.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        let lineWidth = 100 / (this.steps-1);
        for (let i = 0; i < this.steps-1; i++) {
            const line = document.createElement("div");
            line.className="line line-js";
            line.style.width = lineWidth + "%"
            line.style.left = i * lineWidth + "%"
            line.setAttribute("index", i+1);
            outerWrapper.appendChild(line);
        }

        for (let i = 0; i < this.steps; i++) {
            const bubble = document.createElement("div");
            bubble.className="bubble";
            bubble.setAttribute("index", i);
            bubble.role="button";
            bubble.tabIndex="0";
            bubble.setAttribute("aria-label", "Schritt " + (i+1) + " von " + this.steps);
            outerWrapper.appendChild(bubble);

            const button = document.createElement("span");
            button.className="nav-button";
            button.innerText= i+1
            bubble.appendChild(button);
        }

        
    }

    setEvents(){
        let that = this;
        this.bubbles = this.shadowRoot.querySelectorAll('.bubble');
        this.lines = this.shadowRoot.querySelectorAll('.line-js');

        for(let i = 0; i < this.bubbles.length; i++) {
            this.bubbles[i].onclick = (e) => {
                this.setAttribute("current-step", parseInt(e.target.getAttribute("index")) + 1);
            };
            this.bubbles[i].addEventListener('keydown', function(e) {
                if(e.which==13 || e.which==32){
                    e.preventDefault()
                    that.bubbles[i].click()
                }
            });
        }
        this.updateElements()
    }

    updateElements(){    

        for(let i = 0; i < this.bubbles.length; i++) {
            if(i<this.currentStep){
                this.bubbles[i].classList.add("active")
            }else{
                this.bubbles[i].classList.remove("active")
            }
        }
        for(let i = 0; i < this.lines.length; i++) {
            if(i<this.currentStep-1){
                this.lines[i].classList.add("active")
            }else{
                this.lines[i].classList.remove("active")
            }
        }
    }
}
customElements.define('navigation-bar', NavigationBar)
