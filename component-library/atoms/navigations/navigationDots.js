/**
 * Attributes: 
 * steps - (int) number of steps shoiwn in the nav bar
 * current-step - (int) current step on the nav bar
 * src - (json-string) array of src url's for the images of each dot
 * alt - (json-string) array of alt texts for the images
 * 
 * Attribute change callback:
 * current-step
 *
 * How to use:
 * <navigation-dots-buttons src='["assets/img/icon-strom.png", "assets/img/icon-preis.png", "assets/img/icon-co2.png"]' alt='["Minimalistisches Bild einer Lampe", "Minimalistisches Bild von Münzen", "Minimalistisches Bild eines Blattes"]' steps=3 current-step=1 disable-next="false" disable-back="true" back-value="Zurück" next-value="Weiter" next-click-event="custom-event:next-2" back-click-event="custom-event:back-2"></navigation-dots-buttons>
 */

class NavigationDots extends BaseElement {
    /**
     * DOM elements
     */
    bubbles

    /**
     * Attributes
     */
    steps
    currentStep
    src
    alt

    static get observedAttributes() {
        return ['current-step'];
    }

    static get observedAttributesFunction() {
        return {
            'current-step' : function (newValue, that){
                that.currentStep = parseInt(newValue);
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.steps = this.getAttribute("steps");
        this.currentStep = this.getAttribute("current-step");
        this.src = JSON.parse(this.getAttribute("src"));
        this.alt = JSON.parse(this.getAttribute("alt"));
    }

    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/navigations/navigationDots.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        for (let i = 0; i < this.steps; i++) {
            const bubble = document.createElement("div");
            bubble.className="bubble";
            if(this.src){
                bubble.classList.add("image");
            }
            bubble.setAttribute("index", i);
            bubble.role="button";
            // bubble.tabIndex="0";
            // bubble.setAttribute("aria-label", "Schritt " + (i+1) + " von " + this.steps);
            outerWrapper.appendChild(bubble);

            if(this.src){
                const img = document.createElement("img");
                img.src= this.src[i];
                if(this.alt){
                    img.alt= this.alt[i];
                }
                img.setAttribute("index", i);
                bubble.appendChild(img);
            }
        }

        
    }

    setEvents(){
        let that = this;
        this.bubbles = this.shadowRoot.querySelectorAll('.bubble');

        for(let i = 0; i < this.bubbles.length; i++) {
            this.bubbles[i].onclick = (e) => {
                this.setAttribute("current-step", parseInt(e.target.getAttribute("index")) + 1);
            };
            // this.bubbles[i].addEventListener('keydown', function(e) {
            //     if(e.which==13 || e.which==32){
            //         e.preventDefault()
            //         that.bubbles[i].click()
            //     }
            // });
        }
        this.updateElements()
    }

    updateElements(){    

        for(let i = 0; i < this.bubbles.length; i++) {
            if(i+1==this.currentStep){
                this.bubbles[i].classList.add("active")
            }else{
                this.bubbles[i].classList.remove("active")
            }
        }
    }
}
customElements.define('navigation-dots', NavigationDots)
