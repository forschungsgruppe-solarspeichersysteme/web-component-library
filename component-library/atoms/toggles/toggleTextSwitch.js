/**
 * Attributes: 
 * value - (bool) current value of the switch
 * label - (string) label for input
 * options - (json-string) array of two-options to be displayed 
 * aria-label (optional) - (string) invisible label for people who can't see
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <toggle-text-switch value="true" aria-label=""></toggle-text-switch>
 */

class ToggleTextSwitch extends BaseElement {
    /**
     * DOM elements
     */
    input
    wrapper

    /**
     * Attributes
     */
    label
    value
    options
    ariaLabel

    static get observedAttributes() {
        return ['value'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.value = this.getAttribute("value")
        this.options = JSON.parse(this.getAttribute("options"));
        this.ariaLabel = this.getAttribute("aria-label")
    }
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/toggles/toggleTextSwitch.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
 
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

        const innerWrapper = document.createElement("div");
        innerWrapper.className="inner-wrapper";
        outerWrapper.appendChild(innerWrapper);

        const inputWrapper = document.createElement("div");
        inputWrapper.className="switch";
        inputWrapper.tabIndex="0";
        inputWrapper.role="switch";
        inputWrapper.setAttribute("aria-checked", this.value);
        innerWrapper.appendChild(inputWrapper);

        const span = document.createElement("span");
        span.className="slider";
        span.setAttribute("aria-hidden","true")
        inputWrapper.appendChild(span);

        const textWrapper = document.createElement("div");
        textWrapper.className="text-wrapper";
        inputWrapper.appendChild(textWrapper);

        const leftText = document.createElement("span");
        leftText.className="text text-left";
        leftText.innerText = this.options[0];
        textWrapper.appendChild(leftText);

        const rightText = document.createElement("span");
        rightText.className="text text-right";
        rightText.innerText = this.options[1];
        textWrapper.appendChild(rightText);


        // const label = document.createElement("label");
        // label.setAttribute("for", "1") 
        // if(this.ariaLabel !== null){
        //     label.setAttribute("aria-label", this.ariaLabel) 
        // }
        // label.innerText = this.label;
        // innerWrapper.appendChild(label)

    }

    setEvents(){
        let that = this;
        //adding element references 
        this.input = this.shadowRoot.querySelector('input');
        this.wrapper = this.shadowRoot.querySelector('.switch');

        this.wrapper.onclick = (e) => {
            this.setAttribute("value", this.value == 0 ? 1: 0)
        };

        this.wrapper.addEventListener('keydown', function(e) {
            if(e.which==13 || e.which==32){
                e.preventDefault()
                that.wrapper.click()
            }
        });
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            this.value = newValue;
            if(this.value == 1){
                this.wrapper.classList.add("checked");
            }
            else{
                this.wrapper.classList.remove("checked");
            }
                
            // this.wrapper.setAttribute("aria-checked", newValue);
        }
    }
}
customElements.define('toggle-text-switch', ToggleTextSwitch)
