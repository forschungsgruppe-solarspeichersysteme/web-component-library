/**
 * Attributes: 
 * value - (bool) current value of the switch
 * open-text - (string) text to show when toggle is open (value=true)
 * closed-text - (string) text to show when toggle is closed (value=false)
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <toggle-field value="false" toggle-id="result-toggle" open-text="Weniger Ergebnisse" closed-text="Weitere Ergebnisse"></toggle-field>
 */

class ToggleField extends BaseElement {
    /**
     * DOM elements
     */
    outerWrapper
    span
    arrow

    /**
     * Attributes
     */
    value
    openText
    closedText

    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue == 'true';
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value") == 'true';
        this.openText = this.getAttribute("open-text");
        this.closedText = this.getAttribute("closed-text");

    }
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/toggles/toggleField.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
 
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         outerWrapper.tabIndex="0"
         outerWrapper.role="button"
         this.template.content.appendChild(outerWrapper);

         const span = document.createElement("span");
         outerWrapper.appendChild(span);

         const arrow = document.createElement("div");
         arrow.className = "arrow";
         outerWrapper.appendChild(arrow);

        
    }

    setEvents(){
        let that = this;
        //adding element references 
        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.span = this.shadowRoot.querySelector('span');
        this.arrow = this.shadowRoot.querySelector('.arrow');

        this.outerWrapper.onclick = (e) => {
            that.setAttribute("value", !this.value)
        };
        this.outerWrapper.addEventListener('keydown', function(e) {
            if(e.which==13 || e.which==32){
                e.preventDefault()
                that.outerWrapper.click()
            }
        });

        this.updateElements();
    }

    updateElements(){
        if(this.value){
            this.span.innerText = this.openText;
            this.arrow.setAttribute("open", "");
        }else{
            this.span.innerText= this.closedText;
            this.arrow.removeAttribute("open");
        }
    }
}
customElements.define('toggle-field', ToggleField)
