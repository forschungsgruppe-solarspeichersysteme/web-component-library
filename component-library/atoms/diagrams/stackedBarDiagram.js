/**
 * Attributes: 
 * data - (json-string) data attribute for the animation
 *
 * Attribute change callback:
 * data
 *
 * How to use:
 * <bar-result-animation data='{"firstValue": "27.50", "secondValue": "37.27", "firstText": "Mieterstrompreis", "secondText": "max. Preis", "unit" : "ct/kWh", "size": "2em"}'></bar-result-animation>
 */

class StackedBarDiagram extends BaseElement {
    maxBars;
    standardValues = {
        "Strom für Wasserstoffimporte": 2.5,
        "Konventionell": 2.5,
        "Photovoltaik": 2.1,
        "Winkraft aus See": 1,
        "Windkraft an Land": 0.8,
        "Biomasse": 0.4,
        "andere erneuerbare Energien": 0.3
    };
    base = {
        "xAxis": "2020",
        "Strom für Wasserstoffimporte": 2.5,
        "Konventionell": 2.5,
        "Photovoltaik": 2.1,
        "Winkraft aus See": 1,
        "Windkraft an Land": 0.8,
        "Biomasse": 0.4,
        "andere erneuerbare Energien": 0.3
    };
    data = [
    {
        "xAxis": "2030",
        "Strom für Wasserstoffimporte": 3.0,
        "Konventionell": 2.0,
        "Photovoltaik": 3.5,
        "Winkraft aus See": 1.5,
        "Windkraft an Land": 1.2,
        "Biomasse": 0.5,
        "andere erneuerbare Energien": 0.4
    },
    // {
    //     "xAxis": "2035",
    //     "Strom für Wasserstoffimporte": 3.5,
    //     "Konventionell": 1.5,
    //     "Photovoltaik": 4.0,
    //     "Winkraft aus See": 2.0,
    //     "Windkraft an Land": 1.5,
    //     "Biomasse": 0.6,
    //     "andere erneuerbare Energien": 0.5
    // },
    // {
    //     "xAxis": "2040",
    //     "Strom für Wasserstoffimporte": 4.0,
    //     "Konventionell": 1.0,
    //     "Photovoltaik": 4.5,
    //     "Winkraft aus See": 2.5,
    //     "Windkraft an Land": 1.8,
    //     "Biomasse": 0.7,
    //     "andere erneuerbare Energien": 0.6
    // },
    // {
    //     "xAxis": "2045",
    //     "Strom für Wasserstoffimporte": 4.5,
    //     "Konventionell": 0.5,
    //     "Photovoltaik": 5.0,
    //     "Winkraft aus See": 3.0,
    //     "Windkraft an Land": 2.0,
    //     "Biomasse": 0.8,
    //     "andere erneuerbare Energien": 0.7
    // }
]

    labelWrapper = null;

    /** Chart elements */
    chart = null;
    xAxis = null;
    yAxis = null;
    legend = null;

    static get observedAttributes() {
        return ['data', 'active-bar-id'];
    }

    static get observedAttributesFunction() {
        return {
            'data' : function (newValue, that){
                that.data = JSON.parse(newValue);
                console.log("data updating")
                that.setDiagram();
                that.updateElements();
            },
            'active-bar-id' : function (newValue, that){
                that.activeBarId = newValue;
                console.log("active-bar-id updating");
                that.updateActiveBar();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        // this.data = JSON.parse(this.getAttribute("data"));
        this.activeBarId = this.getAttribute("active-bar-id");
        this.maxBars = this.getAttribute("max-bars");
    }

    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/diagrams/stackedBarDiagram.css")
    
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";

        this.template.content.appendChild(outerWrapper);

        var chartdiv = document.createElement("div");
        chartdiv.id="chart-div";
        outerWrapper.appendChild(chartdiv);

        var labelWrapper = document.createElement("div");
        labelWrapper.className="label-wrapper";
        outerWrapper.appendChild(labelWrapper);
    }
    createDiagram(){
        this.root = am5.Root.new(this.shadowRoot.querySelector("#chart-div"));

        var myTheme = am5.Theme.new(this.root);
        
        myTheme.rule("Grid", ["base"]).setAll({
          strokeOpacity: 0.1
        });
        
        
        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        //check if browser is dark or lightmode
        let seperationLineColor = 0x000000;
        let colorTheme = am5themes_Animated.new(this.root)
        if(window.matchMedia('(prefers-color-scheme: dark)').matches){
            seperationLineColor = 0xffffff;
            colorTheme = am5themes_Dark.new(this.root);
        }
        this.root.setThemes([
            colorTheme,
            myTheme
        ]);

        
        
        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        this.chart = this.root.container.children.push(am5xy.XYChart.new(this.root, {
          panX: false,
          panY: false,
          wheelX: "panY",
          wheelY: "zoomY",
          paddingLeft: 0,
          layout: this.root.horizontalLayout
        }));

        var xRenderer = am5xy.AxisRendererX.new(this.root, {});
        this.xAxis = this.chart.xAxes.push(am5xy.CategoryAxis.new(this.root, {
            categoryField: "xAxis",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(this.root, {})
        }));

        xRenderer.grid.template.setAll({
            location: 1
        })
        
        // **Add Vertical Line Between 2020 and 2030**
        var range = this.xAxis.makeDataItem({
            category: this.base.xAxis
        });
        var axisRange = this.xAxis.createAxisRange(range);

        axisRange.get("grid").setAll({
            stroke: am5.color(seperationLineColor), // Set the color of the line (e.g., black)
            strokeWidth: 2,               // Thickness of the line
            // strokeDasharray: [4, 4],      // Dashed line style
            location: 1,                 // Position the line between categories
            strokeOpacity: 1             // Make the color fully opaque
        });
        
        // Optionally, you can add a label to the vertical line
        axisRange.get("grid").set("visible", true);

        this.yAxis = this.chart.yAxes.push(am5xy.ValueAxis.new(this.root, {
            min: 0,
            maxPrecision: 0,
            renderer: am5xy.AxisRendererY.new(this.root, {
                minGridDistance: 40,
                strokeOpacity: 0.1
            })
        }));
        this.yAxis.children.unshift(am5.Label.new(this.root, {
            text: 'Energieversorgung in TWh',
            textAlign: 'center',
            y: am5.p50,
            rotation: -90,
            fontWeight: 'bold'
        }));

        // Add legend
        // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
        this.legend = this.chart.children.push(am5.Legend.new(this.root, {
            centerY: am5.p50,
            y: am5.p50,
            centerX: am5.p100,
            x: am5.p100,
            layout: this.root.verticalLayout,
            reverseChildren: true,
        }));
    }
    setDiagram(){
        var that = this;
        //merge this.base with this.data into one array
        let base = [this.base];
        let data =  base.concat(this.data);
        if(data.length < this.maxBars){
            let placeholderData = [this.placeholderData];
            data = data.concat(placeholderData);
        }

        
        
        // // Add scrollbar
        // // https://www.amcharts.com/docs/v5/charts/xy-chart/scrollbars/
        // chart.set("scrollbarX", am5.Scrollbar.new(this.root, {
        //     orientation: "horizontal"
        // }));

        

        // Create axes
        // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
        
        
        // Move the year labels up
        // xAxis.get("renderer").labels.template.setAll({
        //     y: am5.percent(-20)
        // });
        
        // xAxis.children.push(am5.Label.new(this.root, {
        //     text: 'Vergleichsjahr',
        //     textAlign: 'center',
        //     y: am5.percent(20),
        //     x: am5.percent(9),
        //     fontWeight: 'bold'
        // }));
        // xAxis.children.push(am5.Label.new(this.root, {
        //     text: 'Zieljahr der Klimaneutralität',
        //     textAlign: 'center',
        //     y: am5.percent(20),
        //     x: am5.percent(60),
        //     fontWeight: 'bold'
        // }));
        
        
        
        // this.xAxis.data.removeIndex(0, this.xAxis.data.length);
        this.xAxis.data.setAll(data);
        
       

        that.chart.series.clear();
        that.legend.data.clear();

        Object.entries(this.base).forEach(([key, value]) => {
            console.log(value, key);
            // if xAxis
            if (key === "xAxis") {
                return;
            }
            that.makeSeries(data, key, key);
        });

        // console.log(chart.series._values[0].columns)

        // chart.series.columns.events.on("click", function(event) {
        //     console.log(event.target);
        //     series.columns.each(function(column) {
        //       if (column !== event.target) {
        //         // column.setState("default");
        //         column.isActive = false;
        //       }else{
        //         column.isActive = true;
        //       }
        //     })
        // });
     

        // am5xy.ColumnSeries.columns.template.events.on("click", function(ev) {
        //     console.log("Clicked on a column", ev.target);
        //   });

        // chart.series._values[0].events.on("hit", function(event) {
        //     console.log(event.target);
        //     series.columns.each(function(column) {
        //       if (column !== event.target) {
        //         column.setState("default");
        //         column.isActive = false;
        //       }
        //     })
        // });
        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        this.chart.appear(1000, 100);
        this.xAxis.hide();
    }

     // Add series
    // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
    makeSeries(data, name, fieldName) {
        var that = this;
        var series = that.chart.series.push(am5xy.ColumnSeries.new(that.root, {
            name: name,
            stacked: true,
            xAxis: that.xAxis,
            yAxis: that.yAxis,
            baseAxis: that.xAxis,
            valueYField: fieldName,
            categoryXField: "xAxis"
        }));
        
        series.columns.template.setAll({
            tooltipText: "{name}, {categoryX}: {valueY}",
            tooltipY: am5.percent(90)
        });
        series.data.setAll(data);
        
        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        series.appear();
        
        series.bullets.push(function () {
            return am5.Bullet.new(that.root, {
                sprite: am5.Label.new(that.root, {
                    text: "{valueY}",
                    fill: that.root.interfaceColors.get("alternativeText"),
                    centerY: am5.p50,
                    centerX: am5.p50,
                    populateText: true
                })
                
            });
        });

        series.columns.template.togglable = true;
        // console.log(series.columns.template)

        series.columns.template.events.on("click", function(ev) {
            var column = ev.target;
            var categoryX = column.dataItem.dataContext.xAxis;
            console.log(column);
            console.log("Clicked on a column with category X:", categoryX);
            console.log(that.xAxis);
            // that.updateActiveBar();
            
        
            let child = that.labelWrapper.querySelector("[label-text='"+categoryX+"']").parentNode;
            console.log(child)

            var i = -1;
            while( (child = child.previousSibling) != null ) {
                i++;
            }
            that.setAttribute("active-bar-id", i);

            // that.labelWrapper.querySelectorAll("[label-id]").forEach(function(label) {
            //     console.log(label.getAttribute("label-id"))
            //     if (label.getAttribute("label-id") === categoryX) {
            //         label.classList.add("active");
            //     } else {
            //         label.classList.remove("active");
            //     }
            // });
            // that.labelWrapper.querySelector("[label-id='"+categoryX+"']")
            // Highlight the label with matching text
            // that.xAxis.children.each(function(label) {
            //     console.log(label)
            //     if (label.text === categoryX) {
            //         // label.fill = am5.color("0#ff0000"); // Set the highlight color
            //     } else {
            //         // label.fill = am5.color("0#000000"); // Reset the color of other labels
            //     }
            // });
        });

        that.legend.data.push(series);
    }

    setEvents(){
        this.labelWrapper = this.shadowRoot.querySelector(".label-wrapper");

        this.placeholderData = { ...this.base };
        //remove xAxis from placeholderData
        delete this.placeholderData["xAxis"];
        //foreach placeholderData, add a new data object with 0 values
        for (const [key, value] of Object.entries(this.placeholderData)) {
            this.placeholderData[key] = 0.0;
        }

        this.createDiagram();
        this.setDiagram();
        this.updateElements();
    }
    updateActiveBar(){
        //remove active class from all labels
        this.labelWrapper.querySelectorAll("[label-id]").forEach(function(label) {
            label.classList.remove("active");
        });
        console.log(this.activeBarId, this.labelWrapper.querySelector("[label-id='"+this.activeBarId+"']"))
        //add active class to label with matching id
        this.labelWrapper.querySelector("[label-id='"+this.activeBarId+"']").classList.add("active");
    }

    updateElements(){
        // check if this.shadowRoot.querySelector(".label-wrapper *") is empty
        // if not empty, remove all children
        let that = this;
        let base = [this.base];
        let data =  base.concat(this.data);

        //remove all elements in labelWrapper
        if(this.shadowRoot.querySelector(".label-wrapper") && this.shadowRoot.querySelector(".label-wrapper").children.length > 0){
            this.shadowRoot.querySelector(".label-wrapper").innerHTML = '';
        }

        var frontLabelSpacer = document.createElement("div");
        frontLabelSpacer.className = "front-label-spacer";
        this.labelWrapper.appendChild(frontLabelSpacer);

        let i = 0;
        data.forEach((element, index) => {
            var label = document.createElement("div");
            label.className = "label";
            label.setAttribute("label", element.xAxis);
            this.labelWrapper.appendChild(label);

            var labelText = document.createElement("div");
            labelText.className = that.activeBarId == i ? "label-text active" : "label-text";
            labelText.setAttribute("label-id", i);
            labelText.setAttribute("label-text", element.xAxis);
            labelText.innerHTML = element.xAxis;
            label.appendChild(labelText);

            if(element != this.base){
                const buttonLabel = document.createElement("quantity-button");
                buttonLabel.className="minus";
                buttonLabel.setAttribute("value", "−");
                buttonLabel.setAttribute("custom-aria-label", "Balken entfernen");
                label.appendChild(buttonLabel);
                //add on click button

                buttonLabel.onclick = (e) => {
                    console.log(that.data)
                    //remove element from data
                    let data = that.data;
                    let index = data.indexOf(element);

                    console.log(index);
                    if (index > -1) {
                        data.splice(index, 1);
                    }
                    console.log(that.activeBarId, data.length)
                    if(that.activeBarId >= data.length + 1){
                        that.activeBarId = data.length;
                    }
                    that.setAttribute("data", JSON.stringify(data));
                    
                };

            }
            i++;
        });

        if(data.length < this.maxBars){
            // Add plus label
            var label = document.createElement("div");
            label.className = "label";
            this.labelWrapper.appendChild(label);

            var labelText = document.createElement("div");
            labelText.className = "label-text";
            labelText.innerHTML = "Hinzufügen";
            label.appendChild(labelText);

            const buttonLabel = document.createElement("quantity-button");
            buttonLabel.className="plus";
            buttonLabel.setAttribute("value", "+");
            buttonLabel.setAttribute("custom-aria-label", "Balken hinzufügen");
            label.appendChild(buttonLabel);
            buttonLabel.onclick = (e) => {
                console.log(that.data)
                //add element to data
                let data = that.data;
                let standardValues = that.standardValues;
                //Check all data and if there is a data object with xAxis "Neu", add it like this: "Neu 1", "Neu 2", "Neu 3" etc.
                let i = 1;
                let found = false;
                while(!found){
                    let foundElement = data.find(element => element.xAxis === "Neu (" + i+ ")");
                    if(foundElement){
                        i++;
                    }else{
                        found = true;
                    }
                }
                standardValues["xAxis"] = "Neu (" + i+ ")";
                data.push(standardValues);
                console.log(data)
                that.activeBarId = data.length;
                that.setAttribute("data", JSON.stringify(data));
            };
        }

        var backLabelSpacer = document.createElement("div");
        backLabelSpacer.className = "back-label-spacer";
        this.shadowRoot.querySelector(".label-wrapper").appendChild(backLabelSpacer);
    }
}

customElements.define('stacked-bar-diagram', StackedBarDiagram);
