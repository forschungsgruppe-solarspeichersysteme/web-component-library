/**
 * Attributes: 
 * selected - (bool) if true, the button is checked
 * custom-aria-label - (string) custom aria label for button which a screenreader can see
 * show-tabindex - (bool) if set to true, the raadio buton can be tabbed
 * horizontal - (bool) if set to true, the options will be spread horizontally
 *
 * Attribute change callback:
 * selected
 * show-tabindex
 *
 * How to use:
 * <radio-button selected="false" show-tabindex="true" custom-aria-label="Test"></radio-button>
 */

class RadioButton extends BaseElement {
    /**
     * DOM elements
     */
    radioButton

    /**
     * Attributes
     */
    selected
    ariaLabel
    showTabindex
    label
    horizontal

    static get observedAttributes() {
        return ['selected', 'show-tabindex'];
    }

    static get observedAttributesFunction() {
        return {
            'selected' : function (newValue, that){
                newValue = newValue == "true";
                if(that.selected != newValue){
                    that.selected = newValue
                    that.updateElements();
                }  
            },
            'show-tabindex' : function (newValue, that){
                newValue = newValue == "true";
                if(that.showTabindex != newValue){
                    that.showTabindex = newValue
                    that.updateElements();
                }  
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.selected = this.getAttribute("selected") == "true";
        this.horizontal = this.getAttribute("horizontal") == "true";
        this.label = this.getAttribute("label");
        this.customAriaLabel = this.getAttribute("aria-label");
        this.showTabindex = this.getAttribute("show-tabindex") == "true";
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/radios/radioButton.css")

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        if(this.horizontal === true){
            outerWrapper.classList.add("horizontal")
        }
        this.template.content.appendChild(outerWrapper);

        //HTML elements structure (Indentation stands for the nested structure)
        const radioButton = document.createElement("div");
        radioButton.className="radio-button";
        radioButton.setAttribute("aria-label", this.label)
        radioButton.role="radio"
        outerWrapper.appendChild(radioButton);

        const optionLabel = document.createElement("label");
        optionLabel.className = "option-label";
        optionLabel.innerText = this.label;

        if(this.ariaLabel !== undefined){
            optionLabel.setAttribute("aria-label", this.ariaLabel)
        }

        outerWrapper.appendChild(optionLabel);  
    }

    setEvents(){
        let that = this;
        this.radioButton = this.shadowRoot.querySelector('.radio-button');

        this.radioButton.onclick = (e) => {
            if(!this.selected){
                this.setAttribute("selected", !this.selected)
            }
        };
        this.radioButton.addEventListener('keydown', function(e) {
            if(e.which==13 || e.which==32){
                e.preventDefault()
                that.radioButton.click()
            }
        });
        this.updateElements();
    }

    updateElements(){
        if(this.selected){
            this.radioButton.setAttribute("checked", "")
            this.radioButton.setAttribute("aria-checked", "true")

        }else{
            this.radioButton.removeAttribute("checked")
            this.radioButton.setAttribute("aria-checked", "false")
        }
        if(this.showTabindex){
            this.radioButton.tabIndex = 0
        }else{
            this.radioButton.tabIndex = -1
        }    
    }
}
customElements.define('radio-button', RadioButton)
