/**
 * Attributes: 
 * min - (int) Minimal input value
 * max - (int) Maximum input value
 * step - (int) discrete step distance between min and max (pay attention that step makes sense)
 * value - (int) current value on the input
 * unit (optional) - (string) unit label after input field
 * invisible-label - (string) invisible label for people who can't see
 * error - (string) is true if input has an error
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <number-input error="false" min=0 max=1000 step=100 value=300 unit="kWh" invisible-label="Umschaltfläche Batteriespeicher"></number-input
 */

class NumberInput extends BaseElement {
    /**
     * DOM elements
     */
    input

    /**
     * Attributes
     */
    min
    max
    step
    value
    unit
    invisibleLabel
    error

    static get observedAttributes() {
        return ['value', 'error'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = MathHelper.roundByStep(newValue,that.step)
                // that.input.value = that.numberFormatter.format(that.value);
                that.input.value = that.value.replace('.', ',');
                that.input.classList.remove("warning")
                if(that.error){
                    that.setAttribute("error", "false");
                }
            },
            'error' : function (newValue, that){
                that.error = newValue == "true";
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.min = parseFloat(this.getAttribute("min"))
        this.max = parseFloat(this.getAttribute("max"))
        this.step = parseFloat(this.getAttribute("step"))
        this.value = MathHelper.roundByStep(parseFloat(this.getAttribute("value")),this.step);
        this.unit = this.getAttribute("unit")
        this.invisibleLabel = this.getAttribute("invisible-label")
        this.error = this.getAttribute("error") == "true";
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/inputs/numberInput.css")
        // this.addOwnStylesheet();

         //HTML elements structure (Indentation stands for the nested structure)

        const label = document.createElement("label");
        label.innerText=this.invisibleLabel;
        label.setAttribute("for", "1") 
        this.template.content.appendChild(label) 
         

         const inputField = document.createElement("input");
         inputField.id = "1"
         inputField.className="input-value";
         inputField.type="text";
         inputField.inputMode="numeric"
         inputField.value=this.value.replace('.', ',');
         inputField.step=this.step;
         inputField.min=this.min;
         inputField.max=this.max;
         this.template.content.appendChild(inputField);

         if(this.unit !== null){
            const unitLabel = document.createElement("span");
            unitLabel.className="unit-label";
            unitLabel.innerText=this.unit;
            this.template.content.appendChild(unitLabel) 
         }

         
    }

    setEvents(){
        //adding element references 
        this.input = this.shadowRoot.querySelector('.input-value');
            
        //Event listeners for number input
        this.input.oninput = (e) => {
            if(e.inputType !== "deleteContentForward" && e.inputType !== "deleteContentBackward" && e.inputType !== "insertText"){
                let calcValue = MathHelper.roundByStep(Math.max(Math.min(e.target.value, this.max), this.min), this.step);
                this.setAttribute("value", calcValue)
            }

        };
        this.input.onblur = (e) => {
            let calcValue = MathHelper.roundByStep(Math.max(Math.min(e.target.value.replace(',', '.'), this.max), this.min), this.step);

            if(calcValue == "NaN"){
                // this.input.value = this.value.replace('.', ',');
                this.input.classList.add("warning");
                if(!this.error){
                    this.setAttribute("error", "true");
                }
                return;
            }else{
                this.input.classList.remove("warning")
                if(this.error){
                    this.setAttribute("error", "false");
                }
            }

            if(this.getAttribute("value") != calcValue){
                this.setAttribute("value", calcValue)
            }else if (e.target.value != calcValue){
                this.input.value = calcValue.replace('.', ',');
            }
        };
        
        this.input.addEventListener('keydown', (e) => {
            if(e.which==13){
                e.preventDefault()
                document.activeElement.blur();
            }
        });
    }
}
customElements.define('number-input', NumberInput)
