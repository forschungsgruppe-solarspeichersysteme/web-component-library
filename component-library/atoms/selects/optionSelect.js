/**
 * Attributes: 
 * value - (bool) current value of the switch
 * invisible-label - (string) invisible label for people who can't see
 * options - (json-string) array of the options to be displayed 
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <option-select options='["Schrägdach", "Flachdach", "Fassade"]' value="1"></option-select>
 */

class OptionSelect extends BaseElement {
    /**
     * DOM elements
     */
    select

    /**
     * Attributes
     */
    value
    options
    invisibleLabel

    static get observedAttributes() {
        return ['value'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value")
        this.options = JSON.parse(this.getAttribute("options"));
        this.invisibleLabel = this.getAttribute("invisible-label")
    }
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/selects/optionSelect.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
 
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

         const label = document.createElement("label");
        label.innerText=this.invisibleLabel;
        label.setAttribute("for", "1") 
         outerWrapper.appendChild(label);

         const spacingWrapper = document.createElement("div");
         spacingWrapper.className="spacing-wrapper";
         outerWrapper.appendChild(spacingWrapper);

         const selectWrapper = document.createElement("div");
         selectWrapper.className="select-wrapper";
         outerWrapper.appendChild(selectWrapper);

         const greyWrapper = document.createElement("div");
         greyWrapper.className="grey-wrapper";
         selectWrapper.appendChild(greyWrapper);
        
         const underlyingGreyWrapper = document.createElement("div");
         underlyingGreyWrapper.className="grey-wrapper";
         spacingWrapper.appendChild(underlyingGreyWrapper);

         const select = document.createElement("select");
         select.className="select";
         select.setAttribute("id", "1");
         selectWrapper.appendChild(select);

         for(let i = 0; i < this.options.length; i++){
            const option = document.createElement("option");
            option.text = this.options[i];
            option.value = i;
            if(this.value == i){
                option.setAttribute("selected", "");
            }
            select.appendChild(option);
         }

         const underlyingSelect = document.createElement("select");
         underlyingSelect.setAttribute("id", "1");
         underlyingSelect.setAttribute("disabled", "");
         underlyingSelect.setAttribute("aria-hidden", "true");
         underlyingSelect.setAttribute("tabindex", "-1");
         spacingWrapper.appendChild(underlyingSelect);

         for(let i = 0; i < this.options.length; i++){
            const option = document.createElement("option");
            option.text = this.options[i];
            underlyingSelect.appendChild(option);
         }
    }

    setEvents(){
        let that = this;
        //adding element references 
        this.select = this.shadowRoot.querySelector('select.select');
        this.selectWrapper = this.shadowRoot.querySelector('.select-wrapper');


        this.select.addEventListener("change", function() {
            that.setAttribute("value", this.value);
        });

        // this.wrapper = this.shadowRoot.querySelector('.switch');

        // this.selectWrapper.onclick = (e) => {
        //     console.log("click");
        //     let worked;
        //     if(document.createEvent) { // chrome and safari
        //         console.log("sdfds");
        //         var e = document.createEvent("MouseEvents");
        //         e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        //         worked = this.select.dispatchEvent(e);
        //     }
        //     if(!worked) { // unknown browser / error
        //         alert("It didn't worked in your browser.");
        //     }
        // };

        // this.wrapper.addEventListener('keydown', function(e) {
        //     if(e.which==13 || e.which==32){
        //         e.preventDefault()
        //         that.wrapper.click()
        //     }
        // });
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            this.value = newValue;
            this.select.value = newValue;
            // if(this.value == 1){
            //     this.wrapper.classList.add("checked");
            // }
            // else{
            //     this.wrapper.classList.remove("checked");
            // }
                
            // this.wrapper.setAttribute("aria-checked", newValue);
        }
    }
}
customElements.define('option-select', OptionSelect)
