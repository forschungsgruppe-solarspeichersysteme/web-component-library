/**
 * Attributes: 
 * value - (string) innerText of Button
 * disabled - (bool) is button disabled or not
 * back - (bool) is button arrow showing backwards
 * shake - (bool) shake button
 * 
 * Attribute change callback:
 * disabled, value, shake
 *
 * How to use:
 * <arrow-button value="+" disabled="false" back="true" ></arrow-button>
 */

class ArrowButton extends BaseElement {

    /**
     * DOM elements
     */
    wrapper
    button

    /**
     * Attributes
     */
    value
    disabled
    back
    shake

    static get observedAttributes() {
        return ['disabled', 'value', 'shake'];
    }

    static get observedAttributesFunction() {
        return {
            'disabled' : function (newValue, that){
                that.disabled = newValue == "true";
                that.updateElements()
            },
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            },
            'shake' : function (newValue, that){
                that.shake = newValue == "true";
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.disabled = this.getAttribute("disabled") == "true"; 
        this.shake = this.getAttribute("shake") == "true"; 
        this.back = this.getAttribute("back") == "true";  
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/buttons/arrowButton.css")

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        let arrow = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        // arrow.setAttribute("width", "25");
        // arrow.setAttribute("height", "21");
        arrow.setAttribute("viewBox", "0 0 25 21");

        let path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        path.setAttribute("d", "M2.29171 11.9582H19.5875L14.2938 18.3165C14.1712 18.464 14.0789 18.6341 14.0221 18.8173C13.9653 19.0004 13.9451 19.193 13.9627 19.3839C13.9983 19.7695 14.1856 20.1252 14.4834 20.3728C14.7812 20.6203 15.1651 20.7394 15.5508 20.7038C15.9364 20.6683 16.2921 20.481 16.5396 20.1832L23.8313 11.4332C23.8803 11.3636 23.9242 11.2905 23.9625 11.2144C23.9625 11.1415 24.0355 11.0978 24.0646 11.0248C24.1307 10.8576 24.1653 10.6796 24.1667 10.4998C24.1653 10.32 24.1307 10.1421 24.0646 9.97484C24.0646 9.90192 23.9917 9.85817 23.9625 9.78526C23.9242 9.70922 23.8803 9.6361 23.8313 9.56651L16.5396 0.816507C16.4025 0.651886 16.2308 0.519499 16.0367 0.428759C15.8426 0.33802 15.631 0.291159 15.4167 0.291507C15.076 0.290841 14.7457 0.409513 14.4834 0.626923C14.3357 0.749349 14.2136 0.899704 14.1242 1.06938C14.0347 1.23905 13.9796 1.42471 13.962 1.61572C13.9444 1.80672 13.9646 1.99933 14.0215 2.1825C14.0785 2.36567 14.171 2.53581 14.2938 2.68317L19.5875 9.04151H2.29171C1.90493 9.04151 1.534 9.19515 1.26051 9.46864C0.98702 9.74213 0.833374 10.1131 0.833374 10.4998C0.833374 10.8866 0.98702 11.2575 1.26051 11.531C1.534 11.8045 1.90493 11.9582 2.29171 11.9582Z");
        // path.setAttribute("fill", "#69A500");
        if(this.back){
            outerWrapper.classList.add("back");
            path.setAttribute("d", "M22.7083 11.9582H5.41246L10.7062 18.3165C10.8288 18.464 10.9211 18.6341 10.9779 18.8173C11.0347 19.0004 11.0549 19.193 11.0373 19.3839C11.0017 19.7695 10.8144 20.1252 10.5166 20.3728C10.2188 20.6203 9.83486 20.7394 9.44924 20.7038C9.06362 20.6683 8.70791 20.481 8.46038 20.1832L1.16871 11.4332C1.11965 11.3636 1.07578 11.2905 1.03746 11.2144C1.03746 11.1415 0.964544 11.0978 0.935377 11.0248C0.869276 10.8576 0.834667 10.6796 0.833294 10.4998C0.834667 10.32 0.869276 10.1421 0.935377 9.97484C0.935377 9.90192 1.00829 9.85817 1.03746 9.78526C1.07578 9.70922 1.11965 9.6361 1.16871 9.56651L8.46038 0.816507C8.59749 0.651886 8.7692 0.519499 8.96328 0.428759C9.15736 0.33802 9.36905 0.291159 9.58329 0.291507C9.92404 0.290841 10.2543 0.409513 10.5166 0.626923C10.6643 0.749349 10.7864 0.899704 10.8758 1.06938C10.9653 1.23905 11.0204 1.42471 11.038 1.61572C11.0556 1.80672 11.0354 1.99933 10.9785 2.1825C10.9215 2.36567 10.829 2.53581 10.7062 2.68317L5.41246 9.04151H22.7083C23.0951 9.04151 23.466 9.19515 23.7395 9.46864C24.013 9.74213 24.1666 10.1131 24.1666 10.4998C24.1666 10.8866 24.013 11.2575 23.7395 11.531C23.466 11.8045 23.0951 11.9582 22.7083 11.9582Z");
            // path.setAttribute("fill", "#212427");
            arrow.appendChild(path);
            outerWrapper.appendChild(arrow);
        }else{
            arrow.appendChild(path);
        }
            
        //HTML elements structure (Indentation stands for the nested structure)
        const span = document.createElement("button");
        span.className= "fancy-link";
        span.innerText= this.value;
        outerWrapper.appendChild(span);

        if(!this.back){
            outerWrapper.appendChild(arrow);
        }
        
        
    }

    setEvents(){
        this.wrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.button = this.shadowRoot.querySelector('button');

        this.updateElements();
    }

    updateElements(){
        if(this.disabled){
            this.wrapper.classList.add("disabled")
            this.button.setAttribute("tabIndex", "-1")
        }else{
            this.wrapper.classList.remove("disabled")
            this.button.removeAttribute("tabIndex")
        }
        if(this.shake){
            this.wrapper.classList.add("shake")
        }else{
            this.wrapper.classList.remove("shake")
        }
        this.button.innerText = this.value;
    }
}
customElements.define('arrow-button', ArrowButton)
