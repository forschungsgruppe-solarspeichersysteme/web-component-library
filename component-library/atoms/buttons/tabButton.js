/**
 * Attributes: 
 * value - (string) innerText of Button
 * inactive - (bool) is button inactive or not
 * min-width (optional) - (string) min-width of button
 * 
 * Attribute change callback:
 * inactive
 * value
 *
 * How to use:
 * <tab-button value="6" inactive="false" min-width="5em"></tab-button>
 */

class TabButton extends BaseElement {

    /**
     * DOM elements
     */
    button

    /**
     * Attributes
     */
    value
    inactive
    minWidth

    static get observedAttributes() {
        return ['inactive', 'value'];
    }

    static get observedAttributesFunction() {
        return {
            'inactive' : function (newValue, that){
                that.inactive = newValue == "true";
                that.updateElements()
            },
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.inactive = this.getAttribute("inactive") == "true";
        this.minWidth = this.getAttribute("min-width");
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/buttons/tabButton.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const button = document.createElement("button");
        if(this.type != null){
            button.classList.add(this.type)
        }
        if(this.minWidth != null){
            button.style.minWidth = this.minWidth;
        }
        this.template.content.appendChild(button);

        
    }

    setEvents(){
        this.button = this.shadowRoot.querySelector('button');
        this.updateElements();
    }

    updateElements(){
        if(this.inactive){
            this.button.setAttribute("inactive", "")
        }else{
            this.button.removeAttribute("inactive")
        }
        this.button.innerText = this.value;
    }
}
customElements.define('tab-button', TabButton)
