/**
 * Attributes: 
 * value - (string) innerText of Button
 * inactive - (bool) is button disabled or not
 * custom-aria-label - (string) custom aria label for button
 * 
 * Attribute change callback:
 * inactive
 *
 * How to use:
 * <quantity-button value="+" inactive="false" custom-aria-label="Plus" ></quantity-button>
 */

class QuantityButton extends BaseElement {

    /**
     * DOM elements
     */
    button

    /**
     * Attributes
     */
    value
    inactive
    customAriaLabel

    static get observedAttributes() {
        return ['inactive'];
    }

    static get observedAttributesFunction() {
        return {
            'inactive' : function (newValue, that){
                if(that.inactive  != (newValue == "true")){
                    that.inactive = newValue == "true";
                    that.updateElements();
                }
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.inactive = this.getAttribute("inactive") == "true";
        this.customAriaLabel = this.getAttribute("custom-aria-label");

    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/buttons/quantityButton.css")
            
        //HTML elements structure (Indentation stands for the nested structure)

        const bubble = document.createElement("div");
        bubble.className="bubble";
        bubble.role="button"
        bubble.tabIndex="0"
        bubble.setAttribute("aria-label", this.customAriaLabel)
        this.template.content.appendChild(bubble);

        const button = document.createElement("span");
        button.className="quantity-button";
        button.innerText= this.value;
        bubble.appendChild(button);
    }

    setEvents(){
        this.button = this.shadowRoot.querySelector('.bubble');

        let that = this;

        this.button.addEventListener('keydown', function(e) {
            if(e.which==13 || e.which==32){
                e.preventDefault();
                that.button.click();
            }
        });
        this.button.onclick = (e) => {
            if(that.inactive){
                e.stopPropagation();
            }
        };
        this.updateElements();
    }

    updateElements(){
        if(this.inactive){
            this.button.setAttribute("aria-disabled", "true")
        }else{
            this.button.removeAttribute("aria-disabled")
        }
    }
}
customElements.define('quantity-button', QuantityButton)
