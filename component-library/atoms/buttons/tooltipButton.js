/**
 * Attributes: 
 * value - (string) tooltip text

 * Attribute change callback:
 * value
 *
 * How to use:
 * <tooltip-button value="Tooltip Text"></tooltip-button>
 */

class TooltipButton extends BaseElement {

    /**
     * DOM elements
     */
    wrapper
    button

    /**
     * Attributes
     */
    value
   

    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/buttons/tooltipButton.css")

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        const svgNS = "http://www.w3.org/2000/svg";

        const svg = document.createElementNS(svgNS, "svg");
        svg.setAttributeNS(null, "viewBox", "0 0 48 48");

        const g1 = document.createElementNS(svgNS, "g");
        g1.setAttributeNS(null, "id", "Layer_2");

        const g2 = document.createElementNS(svgNS, "g");
        g2.setAttributeNS(null, "id", "invisible_box");

        const rect = document.createElementNS(svgNS, "rect");
        rect.setAttributeNS(null, "width", "48");
        rect.setAttributeNS(null, "height", "48");
        rect.setAttributeNS(null, "fill", "none");

        g2.appendChild(rect);

        const g3 = document.createElementNS(svgNS, "g");
        g3.setAttributeNS(null, "id", "icons_Q2");

        const path1 = document.createElementNS(svgNS, "path");
        path1.setAttributeNS(null, "d", "M24,2A22,22,0,1,0,46,24,21.9,21.9,0,0,0,24,2Zm0,40A18,18,0,1,1,42,24,18.1,18.1,0,0,1,24,42Z");

        const path2 = document.createElementNS(svgNS, "path");
        path2.setAttributeNS(null, "d", "M24,20a2,2,0,0,0-2,2V34a2,2,0,0,0,4,0V22A2,2,0,0,0,24,20Z");

        const circle = document.createElementNS(svgNS, "circle");
        circle.setAttributeNS(null, "cx", "24");
        circle.setAttributeNS(null, "cy", "14");
        circle.setAttributeNS(null, "r", "2");

        g3.appendChild(path1);
        g3.appendChild(path2);
        g3.appendChild(circle);

        g1.appendChild(g2);
        g1.appendChild(g3);

        svg.appendChild(g1);
        svg.setAttribute("aria-label", "Mehr Informationen");
        svg.setAttribute("role", "button");
        svg.setAttribute("tabindex", "0");

        svg.setAttribute("data-tooltip", this.value);
        outerWrapper.appendChild(svg);
    }

    setEvents(){
        let that = this;
        this.button = this.shadowRoot.querySelector('svg');

        this.button.addEventListener('keydown', function(e) {
            if(e.which==13 || e.which==32){
                e.preventDefault();
                //trigger mouseenter on button
                let event = new MouseEvent('mouseover', {
                    'view': window,
                    'bubbles': true,
                    'cancelable': true
                });
                that.button.dispatchEvent(event);
            }
        });
        //when button focus is left, trigger mouseleave event on button
        this.button.addEventListener('blur', function(e) {
            let event = new MouseEvent('mouseout', {
                'view': window,
                'bubbles': true,
                'cancelable': true
            });
            that.button.dispatchEvent(event);
        });

        var tooltip = new Tooltip({
            theme: "dark",
            delay: 0,
            root: this.shadowRoot
        });
        
        this.updateElements();
    }

    updateElements(){
        this.button.setAttribute("data-tooltip", this.value);
    }
}
customElements.define('tooltip-button', TooltipButton)
