/**
 * Attributes: 
 * type (optional) - (string) which type of button to display (grey)
 * min - (int) Minimal input value
 * max - (int) Maximum input value
 * step - (int) discrete step distance between min and max (pay attention that step makes sense)
 * value - (int) current value on the input
 * unit (optional) - (string) unit label after input field
 * edit - (bool) sets the panel in edit or value mode
 * editable-label (optional) - (string) button text which is shown when in value mode (edit=false)
 * value-label (optional) - (string) button text which is shown when in edit mode (edit=true)
 * loading (optional) - (bool) puts a loading animation above the panel
 * prohibit-edit (optional) - (bool) if set to true, the panel can't be edited
 * 
 * Attribute change callback:
 * value
 * edit
 *
 * How to use:
 * <step-result-panel value="2000" label="Ihr Haushaltsstromverbrauch liegt bei ca." unit="kWh" type="green" min="0" max="5000" step="100" edit="false" editable-label="Wert manuell eintragen" value-label="Wert berechnen"></step-result-panel>
 */

class StepResultPanel extends BaseElement {

    /**
     * DOM elements
     */
    valueElement
    valueWrapper
    inputWrapper
    loadingElement

    /**
     * Attributes
     */
    value
    type
    unit
    label
    max 
    min 
    step
    editableLabel
    valueLabel
    edit
    prohibitEdit
    loading

    static get observedAttributes() {
        return ['value', 'edit', 'loading'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                if(that.edit){
                    that.input.setAttribute("value", that.value)
                }else{
                    that.valueElement.innerText = that.value;
                }
            },
            'edit' : function (newValue, that){
                that.edit = newValue == "true";
                that.updateElements()
            },
            'loading' : function (newValue, that){
                that.loading = newValue == "true";
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label");
        this.unit = this.getAttribute("unit");
        this.value = this.getAttribute("value");
        this.type = this.getAttribute("type");
        this.max = this.getAttribute("max");
        this.min = this.getAttribute("min");
        this.step = this.getAttribute("step");
        this.editableLabel = this.getAttribute("editable-label");
        this.valueLabel = this.getAttribute("value-label");
        this.edit = this.getAttribute("edit") == "true";
        this.prohibitEdit = this.getAttribute("prohibit-edit") == "true";
        this.editableLabel = this.editableLabel !== null ? this.editableLabel : "Wert manuell eintragen";
        this.valueLabel = this.valueLabel !== null ? this.valueLabel : "Wert berechnen";
        this.loading = this.getAttribute("loading") == "true";
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/panels/stepResultPanel.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        if(this.type != null){
            outerWrapper.classList.add(this.type)
        }
        this.template.content.appendChild(outerWrapper);

        const resultLoading = document.createElement("div");
        resultLoading.className="result-loading";
        outerWrapper.appendChild(resultLoading);

        const loadingImage = document.createElement("img");
        loadingImage.className="loader";
        loadingImage.src="component-library/assets/img/loading-image.svg";
        resultLoading.appendChild(loadingImage);

        const contentWrapper = document.createElement("div");
        contentWrapper.className="content-wrapper";
        outerWrapper.appendChild(contentWrapper);

        const labelSpan = document.createElement("span");
        labelSpan.className="label";
        labelSpan.innerText=this.label;
        contentWrapper.appendChild(labelSpan)

        const innerWrapper = document.createElement("div");
        innerWrapper.className="inner-wrapper";
        contentWrapper.appendChild(innerWrapper);

        const valueWrapper = document.createElement("div");
        valueWrapper.className="value-wrapper";
        innerWrapper.appendChild(valueWrapper);

        const value = document.createElement("span");
        value.className="value";
        value.innerText=this.value;
        valueWrapper.appendChild(value)

        const inputWrapper = document.createElement("div");
        inputWrapper.className="input-wrapper";
        innerWrapper.appendChild(inputWrapper)

        if(!this.prohibitEdit){
            const inputField = document.createElement("number-input");
            inputField.className="input step-result-panel-number-input";
            inputField.setAttribute("value", this.value);
            inputField.setAttribute("step", this.step);
            inputField.setAttribute("min", this.min);
            inputField.setAttribute("max", this.max);
            inputField.setAttribute("error", "false");
            inputWrapper.appendChild(inputField)
        }

        if(this.unit != null){
            const unit = document.createElement("span");
            unit.className="unit";
            unit.innerText=this.unit;
            innerWrapper.appendChild(unit)
        }

        if(!this.prohibitEdit){
            const link = document.createElement("htw-button");
            link.setAttribute("type", "link")
            link.setAttribute("value", "")
            contentWrapper.appendChild(link)
        }
    }

    setEvents(){
        this.valueElement = this.shadowRoot.querySelector('.value');
        this.input = this.shadowRoot.querySelector('.input');
        this.valueWrapper = this.shadowRoot.querySelector('.value-wrapper');
        this.inputWrapper = this.shadowRoot.querySelector('.input-wrapper');
        this.loadingElement = this.shadowRoot.querySelector('.result-loading');

        if(!this.prohibitEdit){

            this.createObserver(this.input, "value")

            this.button = this.shadowRoot.querySelector('htw-button');

            this.button.onclick = (e) => {
                this.setAttribute("edit", !this.edit)
            };
        }

        this.updateElements()

    }

    updateElements(){
        if(!this.prohibitEdit){

            if(this.edit){
                this.valueWrapper.setAttribute("removed", "")
                this.valueWrapper.setAttribute("aria-hidden", "true")
                this.inputWrapper.removeAttribute("removed")
                this.input.removeAttribute("tabIndex")
                this.input.removeAttribute("aria-hidden")
                this.button.setAttribute("value", this.valueLabel)
                this.setAttribute("value", this.input.getAttribute("value"))
            }else{
                this.valueWrapper.removeAttribute("removed")
                this.valueWrapper.setAttribute("aria-hidden", "false")
                this.inputWrapper.setAttribute("removed", "")
                this.input.setAttribute("tabIndex", "-1")
                this.input.setAttribute("aria-hidden", "true")
                this.button.setAttribute("value", this.editableLabel)
                this.setAttribute("value", this.valueElement.innerText)
            }
        }
        if(this.loading){
            this.loadingElement.removeAttribute("disabled")
        }else{
            this.loadingElement.setAttribute("disabled", "")
        }
    }
}
customElements.define('step-result-panel', StepResultPanel)
