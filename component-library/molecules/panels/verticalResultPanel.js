/**
 * Attributes: 
 * text - (string) text which is shown under the value and unit
 * animation-atom - (string) animation atom to bind
 * data - (json-string) data attribute for the animation
 * 
 * Attribute change callback:
 * text
 * data
 *
 * How to use:
 * <vertical-result-panel id="bar" animation-atom="bar-result-animation" data='{"firstValue": "27.50", "secondValue": "37.27", "firstText": "Mieterstrompreis", "secondText": "max. Preis", "unit" : "ct/kWh", "size": "2em"}' text="30 ct pro kWh ist der minimal nötige Strompreis. Der maximal mögliche Strompreis liegt bei 33.13 ct pro kWh."></vertical-result-panel>
 */

class VerticalResultPanel extends BaseElement {

    /**
     * DOM elements
     */
    valueElement
    outerWrapper
    resultAnimation
    labelSpan

    /**
     * Attributes
     */
    text 
    animationAtom
    data

    static get observedAttributes() {
        return ['data', 'text'];
    }

    static get observedAttributesFunction() {
        return {
            'data' : function (newValue, that){
                that.data = newValue;
                that.updateElements()
            },
            'text' : function (newValue, that){
                that.text = newValue;
                that.updateElements()
            },
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.data = this.getAttribute("data");
        this.text = this.getAttribute("text");
        this.animationAtom = this.getAttribute("animation-atom");
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/panels/verticalResultPanel.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        let animation = document.createElement(this.animationAtom);
        animation.className="animation"
        animation.setAttribute("data", this.data)

        // img.src = this.src;
        outerWrapper.appendChild(animation);

        const innerWrapper = document.createElement("div");
        innerWrapper.className="inner-wrapper";
        outerWrapper.appendChild(innerWrapper);

        const textWrapper = document.createElement("div");
        textWrapper.className="text-wrapper";
        textWrapper.setAttribute("aria-live", "assertive");
        innerWrapper.appendChild(textWrapper);

        const labelSpan = document.createElement("span");
        labelSpan.className="text";
        // labelSpan.innerText=this.text;
        textWrapper.appendChild(labelSpan)

        
    }

    setEvents(){
        this.resultAnimation = this.shadowRoot.querySelector('.animation');
        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.labelSpan = this.shadowRoot.querySelector('.text');

        this.updateElements()
    }

    updateElements(){
        this.resultAnimation.setAttribute("data", this.data)
        this.labelSpan.innerHTML = this.text;
    }
}
customElements.define('vertical-result-panel', VerticalResultPanel)
