/**
 * Attributes: 
 * min - (int) Minimal slider value
 * max - (int) Maximum slider value
 * step - (int) discrete step distance between min and max (pay attention that step makes sense)
 * value - (int) current value on the slider
 * label (optional) - (string) shows a label above the slider
 * tooltip (optional) - (string) information tooltip added
 * unit - (string) unit to show right of the input field
 * invisible-label - (string) invisible label for people who can't see
 * tick-step (optional) - (int) discrete tick-step distance between min and max
 * number-nth-tick (optional) - (int) number on every n-th tick on the slider
 * horizontal (optional) - (bool) sets the slider horizontal
 * 
 * Attribute change callback:
 * value
 * tooltip
 * 
 * How to use:
 * <vertical-value-slider min=0 max=1000 step=100 value=300 label="Label" unit="kWh" invisible-label="Umschaltfläche Wärmepumpe"></vertical-value-slider>
 */

class VerticalValueSlider extends BaseElement {
    /**
     * DOM elements
     */
    input
    slider
    outerWrapper
    tooltipButton

     /**
     * Attributes
     */
    label
    unit
    min
    max
    step
    value
    invisibleLabel
    tickStep
    numberNthTick
    horizontal
    tempValue
    tooltip

    static get observedAttributes() {
        return ['value', 'horizontal', 'tooltip'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.unit = this.getAttribute("unit")
        this.min = parseInt(this.getAttribute("min"))
        this.max = parseInt(this.getAttribute("max"))
        this.step = parseFloat(this.getAttribute("step"))
        this.value = parseInt(this.getAttribute("value"))
        this.invisibleLabel = this.getAttribute("invisible-label")
        this.tickStep = parseInt(this.getAttribute("tick-step"))
        this.numberNthTick = parseInt(this.getAttribute("number-nth-tick"))
        this.horizontal = this.getAttribute("horizontal") == "true"
        this.tooltip = this.getAttribute("tooltip")
    }

    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("molecules/sliders/verticalValueSlider.css")

      
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         if(this.horizontal){
            outerWrapper.classList.add("horizontal")
        }else{
            outerWrapper.classList.remove("horizontal")
        }
         this.template.content.appendChild(outerWrapper);

        //Add HTML elements
        if(this.label !== null){
            const labelWrapper = document.createElement("div");
            labelWrapper.className="label-wrapper";
            outerWrapper.appendChild(labelWrapper);

            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            labelWrapper.appendChild(labelSpan)

            const tooltipButton = document.createElement("tooltip-button");
            tooltipButton.setAttribute("value", this.tooltip);
            labelWrapper.appendChild(tooltipButton);
            
        }

         const rangeSlider = document.createElement("vertical-range-slider");
         rangeSlider.className="range-slider";
         rangeSlider.setAttribute("value", this.value);
         rangeSlider.setAttribute("step", this.step);
         rangeSlider.setAttribute("min", this.min);
         rangeSlider.setAttribute("max", this.max);
         rangeSlider.setAttribute("drag", false);
         rangeSlider.setAttribute("tick-step", this.tickStep);
         rangeSlider.setAttribute("number-nth-tick", this.numberNthTick);
        rangeSlider.setAttribute("horizontal", this.horizontal);
         outerWrapper.appendChild(rangeSlider)

         const inputField = document.createElement("unit-number-input");
         inputField.setAttribute("invisible-label", this.invisibleLabel);
         inputField.setAttribute("value", this.value);
         inputField.setAttribute("step", this.step);
         inputField.setAttribute("min", this.min);
         inputField.setAttribute("max", this.max);
         inputField.setAttribute("error", "false");
         if(this.unit !== null){
            inputField.setAttribute("unit", this.unit);
         }
         outerWrapper.appendChild(inputField)        
 
        //  const unitLabel = document.createElement("span");
        //  unitLabel.className="unit-label";
        //  unitLabel.innerText=this.unit;
        //  outerWrapper.appendChild(unitLabel)        

         this.shadowRoot.appendChild(this.template.content.cloneNode(true));
    }

    setEvents(){
           //adding element references 
           this.input = this.shadowRoot.querySelector('unit-number-input');
           this.slider = this.shadowRoot.querySelector('vertical-range-slider');
           this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
           this.tooltipButton = this.shadowRoot.querySelector('tooltip-button');

           let that = this
   
           //observe elements for value changes
           var observer = new MutationObserver(function(mutations) {
               mutations.forEach(function(mutation) {
   
                   if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.tempValue) {
                       //equual to mutation.target instanceof NumberInputElement
                       that.tempValue = mutation.target.getAttribute("value")
                        if(that.slider.getAttribute("drag") == "false"){
                            that.setAttribute("value", mutation.target.getAttribute("value"));
                        }
                       switch(mutation.target.constructor){
                           case UnitNumberInput:
                                that.slider.setAttribute("value", mutation.target.getAttribute("value"));
                                break;
                           case VerticalRangeSlider:
                                that.input.setAttribute("value", mutation.target.getAttribute("value"));
                                break;
                           }
                   }
                   if (mutation.type === "attributes" && mutation.attributeName === "drag" && mutation.target.getAttribute("drag") === "false") {
                        that.setAttribute("value", that.tempValue);
                    }
               });
           });
       
           [this.input,this.slider].forEach((node) => {
               observer.observe(node, {
                   attributes: true
               });
           })
           this.updateElements();
    }

    updateElements(){  
        if(this.tooltipButton){  
            if(this.tooltip){
                this.tooltipButton.setAttribute("value", this.tooltip);
                this.tooltipButton.style.display = "block";
            }else{
                this.tooltipButton.style.display = "none";
            }
        }
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            this.value = newValue;
            this.tempValue = newValue;
            this.input.setAttribute("value", newValue);
            this.slider.setAttribute("value", newValue);
        }
        if(propertyName === "horizontal" && oldValue !== null && this.horizontal != (newValue == "true")){
            this.horizontal = newValue == "true";            
            this.slider.setAttribute("horizontal", this.horizontal);
            if(this.horizontal){
                this.outerWrapper.classList.add("horizontal")
            }else{
                this.outerWrapper.classList.remove("horizontal")
            }
        }
        if(propertyName === "tooltip"){
            this.tooltip = newValue;
            if(this.tooltipButton){
                this.updateElements();
            }
        }
    }
}
customElements.define('vertical-value-slider', VerticalValueSlider)
