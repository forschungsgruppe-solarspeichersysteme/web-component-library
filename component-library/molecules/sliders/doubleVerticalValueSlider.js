/**
 * Attributes: 
 * slider-one - (json-string) array of slider one values
 * slider-two - (json-string) array of slider two values
 *
 * label (optional) - (string) shows a label above the slider
 * tooltip (optional) - (string) information tooltip added
 * invisible-label - (string) invisible label for people who can't see
 * selected-slider - (int) selected slider (0 or 1)
 * value - (int) value of the slider
 *
 * Attribute change callback:
 * value
 * selected-slider
 * slider-one
 * slider-two
 * tooltip
 * 
 * How to use:
  <double-vertical-value-slider id="pv_leistung_dachflaeche" label="PV-Leistung" value="50" selected-slider="0" tooltip="dgfh"
                                    slider-one='{"min": "10", "max": "100", "step": "1", "value": "50", "tickStep": "5", "numberNthTick": "2", "unit": "kW", "label" : "Leistung"}'
                                    slider-two='{"min": "50", "max": "500", "step": "5", "value": "250", "tickStep": "50", "numberNthTick": "1", "unit": "m²", "label" : "Fläche"}'
                                ></double-vertical-value-slider>
 */

class DoubleVerticalValueSlider extends BaseElement {
    /**
     * DOM elements
     */
    toggleTextSwitch
    sliderOneElement
    sliderTwoElement
    tooltipButton
    
     /**
     * Attributes
     */
    label
    value
    invisibleLabel
    tooltip
    selectedSlider
    sliderOne
    sliderTwo

    static get observedAttributes() {
        return ['value', 'selected-slider', 'slider-one', 'slider-two', 'tooltip'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                if(that.selectedSlider === "0"){
                    if(that.sliderOne.value !== that.value){
                        that.setAttribute("slider-one", JSON.stringify({min: that.sliderOne.min, max: that.sliderOne.max, step: that.sliderOne.step, value: that.value, unit: that.sliderOne.unit, numberNthTick: that.sliderOne.numberNthTick, tickStep: that.sliderOne.tickStep}));
                    }
                    // if(that.value !== that.sliderOneElement.getAttribute("value")){
                    //     that.sliderOneElement.setAttribute("value", that.value);
                    // }
                }else{
                    if(that.sliderTwo.value !== that.value){
                        that.setAttribute("slider-two", JSON.stringify({min: that.sliderTwo.min, max: that.sliderTwo.max, step: that.sliderTwo.step, value: that.value, unit: that.sliderTwo.unit, numberNthTick: that.sliderTwo.numberNthTick, tickStep: that.sliderTwo.tickStep}));
                    }
                    // if(that.value !== that.sliderTwoElement.getAttribute("value")){
                    //     that.sliderTwoElement.setAttribute("value", that.value);
                    // }
                }
                that.updateElements();
            },
            'selected-slider' : function (newValue, that){
                that.selectedSlider = newValue;
                if(that.selectedSlider === "0"){
                    if(that.value !== that.sliderOneElement.getAttribute("value")){
                        that.setAttribute("value", that.sliderOneElement.getAttribute("value"));
                    }
                }else{
                    if(that.value !== that.sliderTwoElement.getAttribute("value")){
                        that.setAttribute("value", that.sliderTwoElement.getAttribute("value"));
                    }
                }
                that.updateElements();
            },
            'slider-one' : function (newValue, that){
                that.sliderOne = JSON.parse(newValue);
                if(that.selectedSlider === "0"){
                    if(that.sliderOne.value !== that.sliderOneElement.getAttribute("value")){
                        that.setAttribute("value", that.sliderOne.value);
                    }
                }
                that.updateElements();
            },
            'slider-two' : function (newValue, that){
                that.sliderTwo = JSON.parse(newValue);
                if(that.selectedSlider === "1"){
                    if(that.sliderTwo.value !== that.sliderTwoElement.getAttribute("value")){
                        that.setAttribute("value", that.sliderTwo.value);
                    }
                }
                that.updateElements();
            },
            'tooltip' : function (newValue, that){
                that.tooltip = newValue;
                if(this.tooltipButton){
                    this.updateElements();
                }
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.value = parseInt(this.getAttribute("value"))
        this.invisibleLabel = this.getAttribute("invisible-label")
        this.tooltip = this.getAttribute("tooltip")
        this.selectedSlider = this.getAttribute("selected-slider")

        /** 
        * min - (int) Minimal slider value
        * max - (int) Maximum slider value
        * step - (int) discrete step distance between min and max (pay attention that step makes sense)
        * value - (int) current value on the slide
        * unit - (string) unit to show right of the input field
        * label - (string) label for the toggle-text-switch
        * number-nth-tick (optional) - (int) number on every n-th tick on the slider
        * tick-step (optional) - (int) discrete tick-step distance between min and max 
        **/
        this.sliderOne = JSON.parse(this.getAttribute("slider-one"));
        this.sliderTwo = JSON.parse(this.getAttribute("slider-two"));

    }

    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("molecules/sliders/doubleVerticalValueSlider.css")

      
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

        //Add HTML elements
        if(this.label !== null){
            const labelWrapper = document.createElement("div");
            labelWrapper.className="label-wrapper";
            outerWrapper.appendChild(labelWrapper);

            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            labelWrapper.appendChild(labelSpan)

            const tooltipButton = document.createElement("tooltip-button");
            tooltipButton.setAttribute("value", this.tooltip);
            labelWrapper.appendChild(tooltipButton);
            
        }
        const toggleTextSwitch = document.createElement("toggle-text-switch");
        toggleTextSwitch.setAttribute("value", this.selectedSlider);
        toggleTextSwitch.setAttribute("options", JSON.stringify([this.sliderOne.label, this.sliderTwo.label]));
        toggleTextSwitch.setAttribute("toggle-id", "1");
        outerWrapper.appendChild(toggleTextSwitch)

        const sectionDivOne = document.createElement("div");
        sectionDivOne.className="section";
        sectionDivOne.setAttribute("toggle-for", "1");
        sectionDivOne.setAttribute("toggle-value", "0");
        outerWrapper.appendChild(sectionDivOne);

        const secondSectionDivOne = document.createElement("div");
        secondSectionDivOne.className="section";
        sectionDivOne.appendChild(secondSectionDivOne);

        const valueSliderOne = document.createElement("vertical-value-slider");
        valueSliderOne.className="slider-one";
        valueSliderOne.setAttribute("min", this.sliderOne.min);
        valueSliderOne.setAttribute("max", this.sliderOne.max);
        valueSliderOne.setAttribute("step", this.sliderOne.step);
        valueSliderOne.setAttribute("value", this.sliderOne.value);
        if(this.sliderOne.unit !== undefined){
            valueSliderOne.setAttribute("unit", this.sliderOne.unit);
        }
        valueSliderOne.setAttribute("number-nth-tick", this.sliderOne.numberNthTick);
        valueSliderOne.setAttribute("tick-step", this.sliderOne.tickStep);
        secondSectionDivOne.appendChild(valueSliderOne);


        const sectionDivTwo = document.createElement("div");
        sectionDivTwo.className="section";
        sectionDivTwo.setAttribute("toggle-for", "1");
        sectionDivTwo.setAttribute("toggle-value", "1");
        outerWrapper.appendChild(sectionDivTwo);

        const secondSectionDivTwo = document.createElement("div");
        secondSectionDivTwo.className="section";
        sectionDivTwo.appendChild(secondSectionDivTwo);
        
        const valueSliderTwo = document.createElement("vertical-value-slider");
        valueSliderTwo.className="slider-two";
        valueSliderTwo.setAttribute("min", this.sliderTwo.min);
        valueSliderTwo.setAttribute("max", this.sliderTwo.max);
        valueSliderTwo.setAttribute("step", this.sliderTwo.step);
        valueSliderTwo.setAttribute("value", this.sliderTwo.value);
        if(this.sliderTwo.unit !== undefined){
            valueSliderTwo.setAttribute("unit", this.sliderTwo.unit);
        }
        valueSliderTwo.setAttribute("number-nth-tick", this.sliderTwo.numberNthTick);
        valueSliderTwo.setAttribute("tick-step", this.sliderTwo.tickStep);
        secondSectionDivTwo.appendChild(valueSliderTwo);
    
        this.shadowRoot.appendChild(this.template.content.cloneNode(true));
    }

    setEvents(){
        let that = this;
        //adding element references 
        this.toggleTextSwitch = this.shadowRoot.querySelector("toggle-text-switch");
        this.sliderOneElement = this.shadowRoot.querySelector(".slider-one");
        this.sliderTwoElement = this.shadowRoot.querySelector(".slider-two");
        this.tooltipButton = this.shadowRoot.querySelector('tooltip-button');

        //observe elements for value changes
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {

                if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.tempValue) {
                    //equual to mutation.target instanceof NumberInputElement
                    that.tempValue = mutation.target.getAttribute("value")
                    
                    switch(mutation.target.constructor){
                        case ToggleTextSwitch:
                            that.setAttribute("selected-slider",mutation.target.getAttribute("value"));
                            break;
                        case VerticalValueSlider:
                            if((mutation.target == that.sliderOneElement && that.selectedSlider === "0") || (mutation.target == that.sliderTwoElement && that.selectedSlider === "1")){
                                if(that.value !== mutation.target.getAttribute("value")){
                                    that.setAttribute("value", mutation.target.getAttribute("value"));
                                }
                            }
                            break;
                        }
                }
                // if (mutation.type === "attributes" && mutation.attributeName === "drag" && mutation.target.getAttribute("drag") === "false") {
                //     that.setAttribute("value", that.tempValue);
                // }
            });
        });
    
        [this.toggleTextSwitch,this.sliderOneElement, this.sliderTwoElement].forEach((node) => {
            observer.observe(node, {
                attributes: true
            });
        })
        this.updateElements();
    }

    updateElements(){
        this.sliderOneElement.setAttribute("min", this.sliderOne.min);
        this.sliderOneElement.setAttribute("max", this.sliderOne.max);
        this.sliderOneElement.setAttribute("step", this.sliderOne.step);
        if(this.sliderOne.value !== this.sliderOneElement.getAttribute("value")){
            this.sliderOneElement.setAttribute("value", this.sliderOne.value);
        }
        this.sliderOneElement.setAttribute("number-nth-tick", this.sliderOne.numberNthTick);
        this.sliderOneElement.setAttribute("tick-step", this.sliderOne.tickStep);

        this.sliderTwoElement.setAttribute("min", this.sliderTwo.min);
        this.sliderTwoElement.setAttribute("max", this.sliderTwo.max);
        this.sliderTwoElement.setAttribute("step", this.sliderTwo.step);
        if(this.sliderTwo.value !== this.sliderTwoElement.getAttribute("value")){
            this.sliderTwoElement.setAttribute("value", this.sliderTwo.value);
        }
        this.sliderTwoElement.setAttribute("number-nth-tick", this.sliderTwo.numberNthTick);
        this.sliderTwoElement.setAttribute("tick-step", this.sliderTwo.tickStep);

        if (this.selectedSlider === "0") {
            this.sliderOneElement.parentElement.parentElement.removeAttribute("toggle-disabled");
            this.sliderTwoElement.parentElement.parentElement.setAttribute("toggle-disabled", "");
        } else {
            this.sliderOneElement.parentElement.parentElement.setAttribute("toggle-disabled", "");
            this.sliderTwoElement.parentElement.parentElement.removeAttribute("toggle-disabled");
        }

        this.toggleTextSwitch.setAttribute("value", this.selectedSlider);

        if(this.tooltip){
            this.tooltipButton.setAttribute("value", this.tooltip);
            this.tooltipButton.style.display = "block";
        }else{
            this.tooltipButton.style.display = "none";
        }
    }
}
customElements.define('double-vertical-value-slider', DoubleVerticalValueSlider)
