/**
 * Attributes: 
 * options - (json-string) array of options to be displayed 
 * value - (int) decides which option is selected (0 = first element of options sarray)
 * label - (string) shows a label above the radio options
 * horizontal - (bool) if set to true, the options will be spread horizontally
 * tooltip (optional) - (string) information tooltip added
 * 
 * Attribute change callback:
 * value
 * tooltip
 *
 * How to use:
 * <radio-options options='["nachmittags","abends"]' value="0" label="Wann schließen Sie Ihr Elektroauto gewöhnlich zum Laden an?" horizontal="false" ></radio-options>
 */

class RadioOptions extends BaseElement {
    /**
     * DOM elements
     */
    buttons
    lines
    outerWrapper
    tooltipButton

    /**
     * Attributes
     */
    options
    value
    horizontal
    label
    tooltip

    static get observedAttributes() {
        return ['value', 'tooltip'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            },
            'tooltip' : function (newValue, that){
                that.tooltip = newValue;
                if(this.tooltipButton){
                    this.updateElements();
                }
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.options = JSON.parse(this.getAttribute("options"));
        this.value = this.getAttribute("value");
        this.horizontal = this.getAttribute("horizontal") == "true";
        this.label = this.getAttribute("label");
        this.tooltip = this.getAttribute("tooltip");
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("molecules/radios/radioOptions.css")

        //Add HTML elements
        if(this.label !== null){
            const labelWrapper = document.createElement("div");
            labelWrapper.className="label-wrapper";
            this.template.content.appendChild(labelWrapper);

            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            labelWrapper.appendChild(labelSpan);

            const tooltipButton = document.createElement("tooltip-button");
            tooltipButton.setAttribute("value", this.tooltip);
            labelWrapper.appendChild(tooltipButton);
            
        }
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         outerWrapper.role="radiogroup";
         if(this.horizontal === true){
             outerWrapper.classList.add("horizontal")
         }
         this.template.content.appendChild(outerWrapper);
  
         for (let i = 0; i < this.options.length; i++) {
            const radioButton = document.createElement("radio-button");
            // radioButton.setAttribute("role","radio")
            radioButton.setAttribute("selected",true)
            radioButton.setAttribute("label", this.options[i])
            radioButton.setAttribute("horizontal", this.horizontal)
            radioButton.setAttribute("show-tabindex",true)
            radioButton.setAttribute("index", i)
            outerWrapper.appendChild(radioButton);
         }
 
         
    }

    setEvents(){
        this.inputs = this.shadowRoot.querySelectorAll('radio-button');
        this.optionLabels = this.shadowRoot.querySelectorAll('.option-label');
        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.tooltipButton = this.shadowRoot.querySelector('tooltip-button');
        let that = this;

        for(let i = 0; i < this.inputs.length; i++) {
            this.inputs[i].onclick = (e) => {
                if(this.value != e.target.getAttribute("index")){
                    this.setAttribute("value", e.target.getAttribute("index"))
                }
            };
        }

        this.outerWrapper.addEventListener('keydown', function(e) {
            let newValue = null;

            //on arrow up/left
            if(e.which==37 || e.which==38){
                newValue = that.value == 0 ? that.options.length-1 : that.value-1 
            }

            //on arrow down/right
            if(e.which==39 || e.which==40){
                newValue = that.value == that.options.length-1 ? 0 : parseInt(that.value)+1 
            }

            if(newValue !== null){
                e.preventDefault()
                that.setAttribute("value", newValue)
                that.setFocus();
            }
        });

        this.updateElements();
    }

    updateElements(){    
        this.inputs.forEach(input => {
            input.setAttribute("selected", this.value == input.getAttribute("index"));
            input.setAttribute("aria-checked", this.value == input.getAttribute("index"))
            input.setAttribute("show-tabindex", this.value == input.getAttribute("index"));
        });
        if(this.tooltip){
            this.tooltipButton.setAttribute("value", this.tooltip);
            this.tooltipButton.style.display = "block";
        }else{
            if(this.tooltipButton){
                this.tooltipButton.style.display = "none";
            }
        }
    }
    setFocus(){
        this.inputs.forEach(input => {
            if(input.getAttribute("show-tabindex") == "true"){
                SelectorHelper.querySelector(input, ".radio-button").focus();
            }
        });
    }
}
customElements.define('radio-options', RadioOptions)
