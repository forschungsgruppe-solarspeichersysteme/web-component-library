/**
 * Attributes: 
 * disableBack - (bool) disable back button
 * disableNext - (bool) disable next button
 * shakeBack - (bool) shaking back button
 * shakeNext - (bool) shaking next button
 * backValue (optional) - (string) innerText back Button, if no attr is set the text will be "Zurück"
 * nextValue (optional) - (string) innerText next Button, if no attr is set the text will be "Weiter"
 * backClickEvent - (string) click-event which is triggerd on back click
 * nextClickEvent - (string) click-event which is triggerd on next click
 * steps - (int) number of steps
 * current-step - (int) current step
 * src - (json-string) array of src url's for the images of each dot
 * alt - (json-string) array of alt texts for the images
 * 
 * Attribute change callback:
 * disable-back
 * disable-next
 * shake-back
 * shake-next
 * next-value
 * back-value
 * back-click-event
 * next-click-event
 *
 * How to use:
 * <navigation-dots-buttons steps=6 current-step=2 disable-next="false" disable-back="false" back-value="Zurück" next-value="Weiter" next-click-event="sub-view-next" back-click-event="sub-view-back"></navigation-dots-buttons>
 */

class NavigationDotsButtons extends BaseElement {

    /**
     * DOM elements
     */
    backArrow
    navigationDots
    nextArrow

    /**
     * Attributes
     */
    disableBack
    disableNext
    shakeBack
    shakeNext
    backValue
    nextValue
    backClickEvent
    nextClickEvent
    steps
    currentStep
    src
    alt

    /**
     * Helper Variables
     */
    prevBackClickEvent
    prevNextClickEvent

    static get observedAttributes() {
        return ['disable-back', 'disable-next', 'shake-back', 'shake-next', 'next-value', 'back-value', 'next-click-event', 'back-click-event','current-step'];
    }

    static get observedAttributesFunction() {
        return {
            'disable-back' : function (newValue, that){
                that.disableBack = newValue == "true";
                that.updateElements()
            },
            'disable-next' : function (newValue, that){
                that.disableNext = newValue == "true";
                that.updateElements()
            },
            'shake-back' : function (newValue, that){
                that.shakeBack = newValue == "true";
                that.updateElements()
            },
            'shake-next' : function (newValue, that){
                that.shakeNext = newValue == "true";
                that.updateElements()
            },
            'back-value' : function (newValue, that){
                that.backValue = newValue;
                that.updateElements()
            },
            'next-value' : function (newValue, that){
                that.nextValue = newValue;
                that.updateElements()
            },
            'back-click-event' : function (newValue, that){
                that.backClickEvent = newValue;
            },
            'next-click-event' : function (newValue, that){
                that.nextClickEvent = newValue;
            },
            'current-step' : function (newValue, that){
                that.currentStep = newValue;
                that.updateElements()
            },
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.disableBack = this.getAttribute("disable-back") == "true"; 
        this.disableNext = this.getAttribute("disable-next") == "true";  
        this.shakeBack = this.getAttribute("shake-back") == "true"; 
        this.shakeNext = this.getAttribute("shake-next") == "true";  
        this.nextValue = this.getAttribute("next-value");  
        this.backValue = this.getAttribute("back-value");  
        this.nextValue = this.nextValue !== null ? this.nextValue : "Weiter"
        this.backValue = this.backValue !== null ? this.backValue : "Zurück"
        this.nextClickEvent = this.getAttribute("next-click-event");  
        this.backClickEvent = this.getAttribute("back-click-event");  
        this.steps = this.getAttribute("steps");
        this.currentStep = this.getAttribute("current-step");
        this.src = this.getAttribute("src");
        this.alt = this.getAttribute("alt");

    }
    
    build(){

        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/buttons/navigationDotsButtons.css")

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        const backArrow = document.createElement("arrow-button");
        backArrow.className="back-arrow";
        backArrow.setAttribute("value", this.backValue)
        backArrow.setAttribute("shake", this.shakeBack)
        backArrow.setAttribute("back", "true")
        backArrow.setAttribute("disabled", this.disableBack)
        backArrow.setAttribute("click-event", this.backClickEvent);
        outerWrapper.appendChild(backArrow);

        const navigationDots = document.createElement("navigation-dots");
        navigationDots.setAttribute("steps", this.steps);
        navigationDots.setAttribute("current-step", this.currentStep);
        navigationDots.setAttribute("src", this.src);
        navigationDots.setAttribute("alt", this.alt);
        outerWrapper.appendChild(navigationDots);

        const nextArrow = document.createElement("arrow-button");
        nextArrow.className="next-arrow";
        nextArrow.setAttribute("value", this.nextValue)
        nextArrow.setAttribute("shake", this.shakeNext)
        nextArrow.setAttribute("disabled", this.disableNext)
        nextArrow.setAttribute("click-event", this.nextClickEvent);
        outerWrapper.appendChild(nextArrow);
        
        
    }

    setEvents(){
        this.backArrow = this.shadowRoot.querySelector('.back-arrow');
        this.nextArrow = this.shadowRoot.querySelector('.next-arrow');
        this.navigationDots = this.shadowRoot.querySelector('navigation-dots');

        this.createObserver(this.navigationDots, "current-step")
    }

    updateElements(){
        if(this.disableBack){
            this.backArrow.setAttribute("disabled", "true")
        }else{
            this.backArrow.setAttribute("disabled", "false")
        }
        if(this.disableNext){
            this.nextArrow.setAttribute("disabled", "true")
        }else{
            this.nextArrow.setAttribute("disabled", "false")
        }
        if(this.shakeBack){
            this.backArrow.setAttribute("shake", "true")
        }else{
            this.backArrow.setAttribute("shake", "false")
        }
        if(this.shakeNext){
            this.nextArrow.setAttribute("shake", "true")
        }else{
            this.nextArrow.setAttribute("shake", "false")
        }
        this.nextArrow.setAttribute("click-event", this.nextClickEvent);
        this.backArrow.setAttribute("click-event", this.backClickEvent);
        this.backArrow.setAttribute("value", this.backValue);
        this.nextArrow.setAttribute("value", this.nextValue);

        if(this.currentStep != this.navigationDots.getAttribute("current-step")){
            this.navigationDots.setAttribute("current-step", this.currentStep); 
        }
    }
}
customElements.define('navigation-dots-buttons', NavigationDotsButtons)