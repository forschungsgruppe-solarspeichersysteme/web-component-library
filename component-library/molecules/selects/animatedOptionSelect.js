/**
 * Attributes: 
 * options - (json-string) array of options to be displayed 
 * value - (int) decides which option is selected (0 = first element of options sarray)
 * animation - (string) animation name to show
 * additional-animation-value - Additional value for animation
 * label (optional) - (string) shows a label above the slider
 * unit - (string) unit to show right of the input field
 * tooltip (optional) - (string) information tooltip added


 * Attribute change callback:
 * value
 * tooltip
 * 
 * How to use:
 * <animated-option-select options='["15","30","45"]' value="1" label="Um wie viel Grad ist Ihre PV-Anlage geneigt?" unit="°" animation="tiltable-solar-roof"></animated-option-select>
 */

class AnimatedOptionSelect extends BaseElement {
    /**
     * DOM elements
     */
    optionSelect
    animationElement
    tooltipButton

    /**
     * Attributes
     */
    options
    value
    animation
    additionalAnimationValue
    unit
    label
    tooltip

    static get observedAttributes() {
        return ['value', 'additional-animation-value', 'tooltip'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.optionSelect.setAttribute("value", that.value)
                that.updateElements()
            },
            'additional-animation-value' : function (newValue, that){
                that.additionalAnimationValue = newValue;
                that.updateElements()
            },
            'tooltip' : function (newValue, that){
                that.tooltip = newValue;
                if(this.tooltipButton){
                    this.updateElements();
                }
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.options = JSON.parse(this.getAttribute("options"));
        this.value = this.getAttribute("value");
        this.animation = this.getAttribute("animation");
        this.unit = this.getAttribute("unit");
        this.label = this.getAttribute("label")
        this.tooltip = this.getAttribute("tooltip")
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/selects/animatedOptionSelect.css")

        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        if(this.label !== null){
            const labelWrapper = document.createElement("div");
            labelWrapper.className="label-wrapper";
            outerWrapper.appendChild(labelWrapper);

            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            labelWrapper.appendChild(labelSpan)

            const tooltipButton = document.createElement("tooltip-button");
            tooltipButton.setAttribute("value", this.tooltip);
            labelWrapper.appendChild(tooltipButton);
            
        }

         const animationElement = document.createElement(this.animation);
         animationElement.className="animation";
         animationElement.setAttribute("value", this.options[this.value])
         animationElement.setAttribute("additional-animation-value", this.additionalAnimationValue)
         outerWrapper.appendChild(animationElement);

         for(let i = 0; i < this.options.length; i++){
            this.options[i] = this.options[i] + this.unit;
         }

         const optionSelect = document.createElement("option-select");
         optionSelect.setAttribute("value", this.value);
         optionSelect.setAttribute("options", JSON.stringify(this.options));
         outerWrapper.appendChild(optionSelect);
    }

    setEvents(){
        this.optionSelect = this.shadowRoot.querySelector('option-select');
        this.animationElement = this.shadowRoot.querySelector('.animation');
        this.tooltipButton = this.shadowRoot.querySelector('tooltip-button');

        let that = this;
        //observe elements for value changes
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.value) {
                    that.setAttribute("value", mutation.target.getAttribute("value"))
                }
            });
        });
    
        [this.optionSelect].forEach((node) => {
            observer.observe(this.optionSelect, {
                attributes: true
            });
        })
        this.updateElements();
    }

    updateElements(){
        this.animationElement.setAttribute("value", this.options[this.value])
        this.animationElement.setAttribute("additional-animation-value", this.additionalAnimationValue);
        if(this.tooltip){
            this.tooltipButton.setAttribute("value", this.tooltip);
            this.tooltipButton.style.display = "block";
        }else{
            this.tooltipButton.style.display = "none";
        }
    }

    setValue(value){    
        if(this.getAttribute("value") != value){
            this.setAttribute("value", value)
        }
    }
}
customElements.define('animated-option-select', AnimatedOptionSelect)
