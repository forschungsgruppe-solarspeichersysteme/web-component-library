/**
 * Attributes: 
 * value - (int) current value on the input
 * unit - (string) unit label after input field
 * label - (string) shows a label above the input field
 * options - (json-string) array of the options to be displayed 
 * invisible-label - (string) invisible label for people who can't see
 * tooltip (optional) - (string) information tooltip added

 *
 * Attribute change callback:
 * value
 * tooltip
 *
 * How to use:
 * <labeled-option-select label="Montageort" inline="false" options='["Schrägdach", "Flachdach", "Fassade"]' value="1"></labeled-option-select>
 */

class LabeledOptionSelect extends BaseElement {
    /**
     * DOM elements
     */
    select
    tooltipButton

     /**
     * Attributes
     */
    label
    options
    inline
    value 
    tooltip

    static get observedAttributes() {
        return ['value', 'tooltip'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.value = parseInt(this.getAttribute("value"))
        this.inline = this.getAttribute("inline") == "true"
        this.options = this.getAttribute("options");
        this.invisibleLabel = this.getAttribute("invisible-label");
        this.tooltip = this.getAttribute("tooltip");
    }

    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/selects/labeledOptionSelect.css")
        // this.addOwnStylesheet();

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        if(this.inline === true){
            outerWrapper.classList.add("inline")
        }

        const labelWrapper = document.createElement("div");
        labelWrapper.className="label-wrapper";
        outerWrapper.appendChild(labelWrapper);

        //Add HTML elements
        const labelSpan = document.createElement("span");
        labelSpan.className="label";
        labelSpan.innerText=this.label;
        labelWrapper.appendChild(labelSpan)

        const tooltipButton = document.createElement("tooltip-button");
        tooltipButton.setAttribute("value", this.tooltip);
        labelWrapper.appendChild(tooltipButton);
        
        
        const select = document.createElement("option-select");
        select.setAttribute("invisible-label", this.invisibleLabel);
        select.setAttribute("value", this.value);
        select.setAttribute("options", this.options);
        outerWrapper.appendChild(select)        

        this.shadowRoot.appendChild(this.template.content.cloneNode(true));
    }
    setEvents(){
        //adding element references 
        this.select = this.shadowRoot.querySelector('option-select');
        this.tooltipButton = this.shadowRoot.querySelector('tooltip-button');
        let that = this;

        //observe elements for value changes
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.value) {
                that.setAttribute("value", mutation.target.getAttribute("value"));
                }
            });
        });
    
        [this.select].forEach((node) => {
            observer.observe(node, {
                attributes: true
            });
        })
        this.updateElement();
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            this.value = newValue;
            this.updateElement();
        }
        if(propertyName === "tooltip"){
            this.tooltip = newValue;
            if(this.tooltipButton){
                this.updateElement();
            }
        }
    }
    updateElement(){
        this.select.setAttribute("value", this.value);
        if(this.tooltip){
            this.tooltipButton.setAttribute("value", this.tooltip);
            this.tooltipButton.style.display = "block";
        }else{
            this.tooltipButton.style.display = "none";
        }
    }


}
customElements.define('labeled-option-select', LabeledOptionSelect)
