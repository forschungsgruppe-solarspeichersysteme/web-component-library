/**
 * Attributes: 
 * options - (json-string) array of options to be displayed 
 * value - (int) current value on the selector
 * src - (string) source of the image that should be shown
 * img-width - (string) width of individual image
 * label (optional) - (string) shows a label above the radio options
 * alt - (json-string) alt text for images
 * minus-aria-label (optional) - (string) aria-label for minus button
 * plus-aria-label (optional) - (string) aria-label for plus button
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <image-selector id="personen_hh" options='["2300","3000","3500","4000","5000"]' value="1" img-width="2em" src="img/user.svg" alt="Minimalistischer Mensch" label="Wie viele Personen leben in Ihrem Haushalt?"></image-selector>
 */

class ImageSelector extends BaseElement {
    /**
     * DOM elements
     */
    plusButton
    minusButton
    imgWrappers

    /**
     * Attributes
     */
    src
    options
    value
    imgWidth
    label
    alt
    minusAriaLabel
    plusAriaLabel

    static get observedAttributes() {
        return ['value'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.options = JSON.parse(this.getAttribute("options"));
        this.value = parseFloat(this.getAttribute("value"))
        this.src = this.getAttribute("src");
        this.alt = JSON.parse(this.getAttribute("alt"));
        this.label = this.getAttribute("label")
        this.imgWidth = this.getAttribute("img-width")
        this.minusAriaLabel = this.getAttribute("minus-aria-label")
        this.plusAriaLabel = this.getAttribute("plus-aria-label")
    }

    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/selectors/imageSelector.css")

        //Add HTML elements
        if(this.label !== null){
            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            this.template.content.appendChild(labelSpan)
    	}

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        //HTML elements structure (Indentation stands for the nested structure)
        const minusButton = document.createElement("quantity-button");
        minusButton.className="minus";
        minusButton.setAttribute("value", "−")
        minusButton.setAttribute("custom-aria-label", this.minusAriaLabel ? this.minusAriaLabel : "Abziehen")
        minusButton.setAttribute("inactive", this.value == 0 ? "true" : "false")
        outerWrapper.appendChild(minusButton);

        for (let i = 0; i < this.options.length; i++) {
            const imgWrapper = document.createElement("div");
            imgWrapper.className="img-wrapper";
            imgWrapper.role = "button";
            imgWrapper.setAttribute("tabindex", "0")
            imgWrapper.setAttribute("aria-label", this.alt[i]);
            imgWrapper.setAttribute("value", i)
            outerWrapper.appendChild(imgWrapper);

            const img = document.createElement("img");
            img.src = this.src;
            img.alt = this.alt[i];
            img.style.width = this.imgWidth;
            imgWrapper.appendChild(img);
        }

        const plusButton = document.createElement("quantity-button");
        plusButton.className="plus";
        plusButton.setAttribute("value", "+")
        plusButton.setAttribute("custom-aria-label", this.plusAriaLabel ? this.plusAriaLabel : "Hinzufügen")
        plusButton.setAttribute("inactive", (this.options.length - 1) == this.value ? "true" : "false")
        outerWrapper.appendChild(plusButton);

        
    }

    setEvents(){
        let that = this;
         //adding element references 
         this.imgWrappers = this.shadowRoot.querySelectorAll('.img-wrapper');
         this.plusButton = this.shadowRoot.querySelector('.plus');
         this.minusButton = this.shadowRoot.querySelector('.minus');
 
         this.minusButton.onclick = (e) => {
             this.setAttribute("value", this.value - 1)
         };
         this.plusButton.onclick = (e) => {
             this.setAttribute("value", this.value + 1)
         };
 
         for(let i = 0; i < this.imgWrappers.length; i++) {
             this.imgWrappers[i].onclick = (e) => {
                 this.setAttribute("value", e.target.getAttribute("value"))
 
             };
             this.imgWrappers[i].addEventListener('keydown', function(e) {
                //on enter
                if(e.which==13){
                    that.setAttribute("value", e.target.getAttribute("value"))
                }
            });
         }
       
 
         this.updateElements();
    }

    updateElements(){
        for(let i = 0; i < this.imgWrappers.length; i++) {
            if(this.imgWrappers[i].getAttribute("value") <= this.value){
                this.imgWrappers[i].classList.add("selected")
            }else{
                this.imgWrappers[i].classList.remove("selected")
            }
        }   
        this.plusButton.setAttribute("inactive", (this.options.length - 1) == this.value ? "true" : "false")
        this.minusButton.setAttribute("inactive", this.value == 0 ? "true" : "false")

    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null && this.value != newValue){

            this.value = parseFloat(newValue);
            this.updateElements();
        }
    }
}
customElements.define('image-selector', ImageSelector)
