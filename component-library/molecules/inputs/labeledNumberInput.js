/**
 * Attributes: 
 * min - (int) Minimal input value
 * max - (int) Maximum input value
 * step - (int) discrete step distance between min and max (pay attention that step makes sense)
 * value - (int) current value on the input
 * unit - (string) unit label after input field
 * label - (string) shows a label above the input field
 * inline (optional) - (bool) is set to true, the label, input field and unit label on the same horizontal line
 * invisible-label - (string) invisible label for people who can't see
 * tooltip (optional) - (string) information tooltip added
 * input-component (optional) - (string) input component to use
 *
 * Attribute change callback:
 * value
 * tooltip
 *
 * How to use:
 * <labeled-number-input min=0 max=1000 step=100 value=300 unit="kWh" label="Test" inline="true" invisible-label="Test"></labeled-number-input>
 */

class LabeledNumberInput extends BaseElement {
    /**
     * DOM elements
     */
    input
    slider
    tooltipButton

     /**
     * Attributes
     */
    label
    unit
    min
    max
    step
    value
    inline
    invisibleLabel
    tooltip
    inputComponent

    static get observedAttributes() {
        return ['value','tooltip'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.unit = this.getAttribute("unit")
        this.min = parseInt(this.getAttribute("min"))
        this.max = parseInt(this.getAttribute("max"))
        this.step = parseFloat(this.getAttribute("step"))
        this.value = parseInt(this.getAttribute("value"))
        this.inline = this.getAttribute("inline") == "true"
        this.invisibleLabel = this.getAttribute("invisible-label")
        this.tooltip = this.getAttribute("tooltip")
        this.inputComponent = this.getAttribute("input-component")

    }

    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/inputs/labeledNumberInput.css")
        // this.addOwnStylesheet();

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        if(this.inline === true){
            outerWrapper.classList.add("inline")
        }

        const labelWrapper = document.createElement("div");
        labelWrapper.className="label-wrapper";
        outerWrapper.appendChild(labelWrapper);

        //Add HTML elements
        const labelSpan = document.createElement("span");
        labelSpan.className="label";
        labelSpan.innerText=this.label;
        labelWrapper.appendChild(labelSpan);

        const tooltipButton = document.createElement("tooltip-button");
        tooltipButton.setAttribute("value", this.tooltip);
        labelWrapper.appendChild(tooltipButton);
        
        const inputField = document.createElement(this.inputComponent !== null ? this.inputComponent : "number-input");
        inputField.setAttribute("invisible-label", this.invisibleLabel);
        inputField.className= this.inputComponent !== null ? "input" : "labeled-number-input-number-input input";
        inputField.setAttribute("value", this.value);
        inputField.setAttribute("step", this.step);
        inputField.setAttribute("min", this.min);
        inputField.setAttribute("max", this.max);
        inputField.setAttribute("unit", this.unit);
        inputField.setAttribute("error", "false");
        outerWrapper.appendChild(inputField)        

        this.shadowRoot.appendChild(this.template.content.cloneNode(true));
    }
    setEvents(){
        //adding element references 
        this.input = this.shadowRoot.querySelector('.input');
        this.tooltipButton = this.shadowRoot.querySelector('tooltip-button');
        let that = this;

        //observe elements for value changes
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.value) {
                that.setAttribute("value", mutation.target.getAttribute("value"));
                }
            });
        });
    
        [this.input].forEach((node) => {
            observer.observe(node, {
                attributes: true
            });
        })

        this.updateElements();
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            this.value = newValue;
            this.input.setAttribute("value", newValue);
        }
        if(propertyName === "tooltip"){
            this.tooltip = newValue;
            if(this.tooltipButton){
                this.updateElements();
            }
        }
    }

    updateElements(){
        if(this.tooltip){
            this.tooltipButton.setAttribute("value", this.tooltip);
            this.tooltipButton.style.display = "block";
        }else{
            this.tooltipButton.style.display = "none";
        }
    }
}
customElements.define('labeled-number-input', LabeledNumberInput)
