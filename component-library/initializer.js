// The Initializer class encapsulates the initialization logic for various components and files.
class Initializer{

    // Define the base path for loading resources.
    static mainPath = "component-library/"

    // Define an array of JavaScript files to be preloaded.
    preloadJsFiles = [
        // ---- Helpers ----
        "helpers/mathHelper.js",
        "helpers/formatHelper.js",
        "helpers/selectorHelper.js",
        "helpers/urlHelper.js",

        "assets/js/clickHandler.js",
        "assets/js/tabHandler.js",
        "assets/js/observerHandler.js",
        "assets/js/viewHandler.js",
        "assets/js/subViewNavbarHandler.js",
        "assets/js/tippy/popperjsCore.js",
        "assets/js/tooltipHandler.js",
        "atoms/baseElement.js",

        //amCharts
        "assets/js/amCharts/index.js",
        "assets/js/amCharts/xy.js",
        "assets/js/amCharts/themes/Animated.js",
        "assets/js/amCharts/themes/Dark.js",
        "assets/js/amCharts/locales/de_DE.js",
        "assets/js/amCharts/geodata/worldLow.js",
        // "assets/js/amCharts/fonts/notosams-sc.js"
    ]

    // Define arrays of JavaScript to be loaded.
    jsFiles = [
        //Tippy needs to be loaded after popper.js
        "assets/js/tippy/tippy.js",

        // ---- Atoms ----
        // Animations
        "atoms/animations/tiltableSolarRoof.js",
        "atoms/animations/compassSolarRoof.js",
        "atoms/animations/compassFacadeSolar.js",
        "atoms/animations/compassFlatSolarRoof.js",
        "atoms/animations/fillResultAnimation.js",
        "atoms/animations/circleResultAnimation.js",
        "atoms/animations/flatSolarRoof.js",
        "atoms/animations/facadeSolar.js",
        "atoms/animations/percentCircleResultAnimation.js",
        "atoms/animations/barResultAnimation.js",
        "atoms/animations/logoBackgroundAnimation.js",
        // Buttons
        "atoms/buttons/quantityButton.js",
        "atoms/buttons/arrowButton.js",
        "atoms/buttons/htwButton.js",
        "atoms/buttons/imageButton.js",
        "atoms/buttons/tabButton.js",
        "atoms/buttons/tooltipButton.js",
        //Diagrams
        "atoms/diagrams/stackedBarDiagram.js",
        // Input
        "atoms/inputs/numberInput.js",
        "atoms/inputs/unitNumberInput.js",
        // Slider
        "atoms/sliders/rangeSlider.js",
        "atoms/sliders/stepSlider.js",
        "atoms/sliders/verticalRangeSlider.js",
        // Toggle
        "atoms/toggles/toggleField.js",
        "atoms/toggles/toggleSwitch.js",
        "atoms/toggles/toggleTextSwitch.js",
        // Navigations,
        "atoms/navigations/navigationBar.js",
        "atoms/navigations/navigationDots.js",
        // Radio
        "atoms/radios/radioButton.js",
        //Selects
        "atoms/selects/optionSelect.js",


        // ---- Molecules ----
        // Buttons
        "molecules/buttons/navigationButtons.js",
        "molecules/buttons/navigationDotsButtons.js",
        "molecules/buttons/tabButtonPanel.js",
        // Input
        "molecules/inputs/labeledNumberInput.js",
        // Panel
        "molecules/panels/resultPanel.js",
        "molecules/panels/stepResultPanel.js",
        "molecules/panels/verticalResultPanel.js",

        // Slider
        "molecules/sliders/valueSlider.js",
        "molecules/sliders/animatedStepSlider.js",
        "molecules/sliders/verticalValueSlider.js",
        "molecules/sliders/doubleVerticalValueSlider.js",
        // Radio
        "molecules/radios/radioOptions.js",
        "molecules/radios/imageRadioOptions.js",
        // Selector
        "molecules/selectors/imageSelector.js",
        //Selects
        "molecules/selects/labeledOptionSelect.js",
        "molecules/selects/animatedOptionSelect.js",

    ]

    // Define arrays of CSS to be loaded.
    cssFiles = [
        "assets/css/main.css",
        "assets/css/tabHandler.css",
        "assets/css/viewHandler.css",
        "assets/css/observerHandler.css",
        "assets/css/tooltip.css",
    ]

    // Initialize the application.
    async init(){
        // Load files of bridge
        await this.#loadFiles(["abstractBridge.js"]);
        await this.#loadFiles(["../bridge.js"]);
        this.bridge = new Bridge(this);

        // Load preloaded JavaScript files.
        await this.#loadFiles(this.preloadJsFiles.concat(this.bridge.preloadJsFiles));

        // Load main JavaScript files.
        await this.#loadFiles(this.jsFiles.concat(this.bridge.jsFiles));

        // Load CSS files.
        await this.#loadFiles(this.cssFiles.concat(this.bridge.cssFiles), true);   

      

        // Create instances of various handlers and components.
        this.observerHandler = new ObserverHandler(this);

        // Initialize the Bridge.
        this.bridge.init(); 

        this.tabHandler = new TabHandler(this);
        this.viewHandler = new ViewHandler(this);
        this.viewHandler.init();
        this.clickHandler = new ClickHandler(this); 
        this.subViewNavBarHandler = new SubViewNavBarHandler(this);

        // Notify the bridge that the application has finished loading.
        this.bridge.finishedLoading(); 
}

    // Load files dynamically (JavaScript or CSS).
    async #loadFiles(files, isCSS = false) {
        const promiseArray = []; // create an array for promises
    
        for (let url of files) {
    
            promiseArray.push(new Promise(resolve => {
                let element;

                if(isCSS){
                    // Create a link element for CSS files.
                    element = document.createElement("link");
                    element.rel="stylesheet";
                    element.href= Initializer.mainPath + url;
                    document.getElementsByTagName('head')[0].appendChild(element);
                }else{
                    // Create a script element for JavaScript files.
                    element = document.createElement('script');
                    element.src = Initializer.mainPath + url;
                    document.head.appendChild(element);
                }
    
                // Resolve the promise when the file is loaded.
                element.onload = function() {
                    resolve();
                };
    
            }));
        }

        // Wait for all the files to be loaded
        await Promise.all(promiseArray);
    }
}

// Self-invoking function to create an instance of Initializer and call its init() method.
(function() {
    let initializer = new Initializer()
    initializer.init();
})();