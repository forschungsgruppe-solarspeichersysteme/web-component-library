Tooltip = function(options) {
    delay = options.delay || 0,
    dist  = options.distance || 5;
    root = options.root || document.body;

    // Recursive function to traverse all nodes and their shadow roots
    function traverseNodes(root) {
        var nodeIterator = document.createNodeIterator(
            root,
            NodeFilter.SHOW_ELEMENT,
            { acceptNode: function(node) { return NodeFilter.FILTER_ACCEPT; } },
            false
        );

        var currentNode;
        while (currentNode = nodeIterator.nextNode()) {
            // Check if the current node has a shadow root
            if (currentNode.shadowRoot) {
                // If it does, traverse its shadow root as well
                traverseNodes(currentNode.shadowRoot);
            }
            // Also check the current node itself
            if (currentNode.hasAttribute('data-tooltip')) {
                let tippyInstance = tippy(currentNode, {
                    content: currentNode.getAttribute('data-tooltip'),
                });

                // Create a MutationObserver instance to watch for changes in 'data-tooltip'
                let observer = new MutationObserver((function(node) {
                    return function(mutations) {
                        mutations.forEach(function(mutation) {
                            if (mutation.type == 'attributes' && mutation.attributeName == 'data-tooltip') {
                                // Update tippy content when 'data-tooltip' changes
                                tippyInstance.setContent(node.getAttribute('data-tooltip'));
                            }
                        });
                    };
                })(currentNode));

                // Start observing the target node for configured mutations
                observer.observe(currentNode, { attributes: true });
            }
        }
    }

    // Start traversing from the body
    traverseNodes(root);
};