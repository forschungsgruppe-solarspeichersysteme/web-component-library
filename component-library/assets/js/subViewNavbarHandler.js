// The ViewHandler class manages the switching of main and sub views, as well as event listeners for buttons.
class SubViewNavBarHandler {
    // Declare instance variables.
    initializer;

    navigationBar;
    switchedNavigationBar = false;

    // Constructor to set up the ViewHandler.
    constructor(initializer) {
        this.initializer = initializer;
        this.initNavigationBarListener();
    }

    // Register the navigation bar element.
    initNavigationBarListener() {
        let that = this;
        this.navigationBar = document.querySelector("navigation-bar");

        if(this.navigationBar){
            // Create a MutationObserver to observe attribute changes.
            var observer = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    if (mutation.type === "attributes" && mutation.attributeName === "current-step") {
                        that.initializer.subViewNavBarHandler.switchSubViewByNavBarIndex(mutation.target.getAttribute("current-step"));
                    }
                });
            });
    
            // Observe changes for each custom element.
            observer.observe(this.navigationBar, {
                attributes: true
            });
        }

    }

    /**
     * Switch subviews based on navigation bar press.
     * 
     * @param {int} index 
     */
    switchSubViewByNavBarIndex(index) {
        if (!this.switchedNavigationBar) {
            this.initializer.viewHandler.switchSubView(index - 1);
        } else {
            this.switchedNavigationBar = false;
        }
    }
}
