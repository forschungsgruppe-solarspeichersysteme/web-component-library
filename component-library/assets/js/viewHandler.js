// The ViewHandler class manages the switching of main and sub views, as well as event listeners for buttons.
class ViewHandler {
    // Declare instance variables.
    initializer;

    // NodeList of main views.
    mainViews = {};

    // Array with subview NodeLists on the index of mainViews.
    subViews = [];

    currentMainViewIndex = 0;
    currentSubViewIndex = 0;

    mainViewClassName = "main-view";
    subViewClassName = "sub-view";


    // Constructor to set up the ViewHandler.
    constructor(initializer) {
        this.initializer = initializer;
    }

    // Initialize the ViewHandler by registering views and setting up event listeners.
    init() {
        this.registerMainViews();
        this.registerSubViews();
    }

    // Register main views based on the specified class name.
    registerMainViews() {
        this.mainViews = document.querySelectorAll(" ." + this.mainViewClassName);
        this.switchMainView(this.currentMainViewIndex);
    }

    // Register subviews for each main view.
    registerSubViews() {
        for (let i = 0; i < this.mainViews.length; i++) {
            let mainView = this.mainViews[i];
            this.subViews[i] = mainView.querySelectorAll("." + this.subViewClassName);
        }
        if(this.mainViews.length > 0){
            this.switchSubView(this.currentSubViewIndex);
        }
    }

    /**
     * Switch to another main view based on the id attribute.
     * 
     * @param {string} id 
     */
    switchMainViewById(id, flowEscape) {
        let index;

        for (let i = 0; i < this.mainViews.length; i++) {
            let iterateView = this.mainViews[i];
            if (iterateView.id == id) {
                index = i;
                break;
            }
        }
        this.switchMainView(index, flowEscape);
    }

    /**
     * Switch to another main view based on the index (position is determined by placement in DOM).
     * 
     * @param {int} index 
     */
    switchMainView(index, flowEscape = false) {
        // Check for custom main-view switch.
        if (this.initializer.bridge.checkForCustomMainViewSwitch(index, flowEscape)) {
            return;
        }

        for (let i = 0; i < this.mainViews.length; i++) {
            let iterateView = this.mainViews[i];
            if (i == index) {
                iterateView.setAttribute("selected", "");
                this.currentMainViewIndex = i;
            } else {
                iterateView.removeAttribute("selected");
            }
        }

       this.initializer.bridge.afterSwitchMainView(flowEscape);
    }

    /**
     * Switch to another sub view based on the id attribute.
     * 
     * @param {*} id 
     * @param {bool} flowEscape If set to true, navigation to another subview is hidden.
     */
    switchSubViewById(id, flowEscape) {
        let index;

        for (let i = 0; i < this.subViews[this.currentMainViewIndex].length; i++) {
            let iterateView = this.subViews[this.currentMainViewIndex][i];
            if (iterateView.id == id) {
                index = i;
                break;
            }
        }
        this.switchSubView(index, flowEscape);
    }

    /**
     * Switch to another sub view based on the index (position is determined by placement in DOM).
     * 
     * @param {*} index 
     * @param {*} flowEscape If set to true, navigation to another subview is hidden.
     */
    switchSubView(index, flowEscape = false) {
        // Check for custom sub-view switch.
        if (this.initializer.bridge.checkForCustomSubViewSwitch(index, flowEscape)) {
            return;
        }

        for (let i = 0; i < this.subViews[this.currentMainViewIndex].length; i++) {
            let iterateView = this.subViews[this.currentMainViewIndex][i];
            if (i == index) {
                iterateView.setAttribute("selected", "");
                this.currentSubViewIndex = i;
            } else {
                iterateView.removeAttribute("selected");
            }
        }
        this.initializer.bridge.afterSwitchSubView(flowEscape);
    }
}
