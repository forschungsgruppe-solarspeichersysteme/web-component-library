// The ObserverHandler class manages various tasks related to handling HTML elements.
class ObserverHandler {
    // Declare an instance variable for the Initializer.
    initializer;

    // Constructor that takes an Initializer instance as a parameter.
    constructor(initializer) {
        this.initializer = initializer;

        // Perform initial checks and set up observers.
        this.checkToggles();
        this.observeElements();
    }

    // Observe custom elements for attribute changes using MutationObserver.
    observeElements() {
        let that = this;

        // Get all elements in the document.
        const elements = document.querySelectorAll('*');

        // Convert NodeList to an array.
        const elementArray = Array.from(elements);

        // Map to node names.
        const nodeNames = elementArray.map(element => element.nodeName.toLowerCase());

        // Filter by which ones are registered as custom elements.
        const allCustomElementNames = nodeNames.filter(customElements.get.bind(customElements));
        let selector = "";
        allCustomElementNames.forEach((name) => {
            selector += name + ", ";
        });
        selector = selector.substring(0, selector.length - 2);
        console.log("Used custom elements: " + selector);

        // Select all custom elements based on their names.
        let customElementsList = document.querySelectorAll(selector);

        // Create a MutationObserver to observe attribute changes.
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                // Handle attribute changes for specific custom elements.
                if (mutation.type === "attributes" && mutation.attributeName !== "tabindex") {
                    switch (mutation.target.constructor) {
                        case ToggleSwitch:
                        case ToggleField:
                            that.checkToggleFor(mutation.target);
                            break;
                        case StepResultPanel:
                            that.checkEditFor(mutation.target);
                            break;
                        case ToggleTextSwitch:
                        default:
                            that.checkToggleTextFor(mutation.target);
                            break;
                    }
                }
            });
        });

        // Observe changes for each custom element.
        customElementsList.forEach((node) => {
            observer.observe(node, {
                attributes: true
            });
        });
    }

    // Check if linked toggles are on the same position
    checkToggles() {
        // Select all toggle switches and toggle fields.
        const switches = document.querySelectorAll('toggle-switch, toggle-field');

        // Check each toggle switch or toggle field for consistency.
        for (let i = 0; i < switches.length; i++) {
            this.checkToggleFor(switches[i]);
        }

        // Select all step result panels.
        const panels = document.querySelectorAll('step-result-panel');

        // Check each step result panel for consistency.
        for (let i = 0; i < panels.length; i++) {
            this.checkEditFor(panels[i]);
        }

         // Select all toggle switches and toggle fields.
        let rest = document.querySelectorAll('[toggle-id]:not(step-result-panel):not(toggle-switch):not(toggle-field)');

         // Check each toggle switch or toggle field for consistency.
         for (let i = 0; i < rest.length; i++) {
             this.checkToggleTextFor(rest[i]);
         }
    }

    // Checks if toggle fields are disabled if necessary and other toggles with the same toggle-id have the correct value
    checkToggleFor(toggle) {
        let forAttr = toggle.getAttribute("toggle-id");
        let toggableElements = document.querySelectorAll('[toggle-for=' + forAttr + ']');
        let sameToggles = document.querySelectorAll('[toggle-id=' + forAttr + ']');

        // Check linked toggle fields for disabled status.
        for (let i = 0; i < toggableElements.length; i++) {
            this.handleToggleDisable(toggableElements[i], toggle.getAttribute("value") != "true");
        }

        // Check other toggles with the same toggle-id for consistent values.
        for (let i = 0; i < sameToggles.length; i++) {
            if (sameToggles[i].getAttribute("value") !== toggle.getAttribute("value")) {
                sameToggles[i].setAttribute("value", toggle.getAttribute("value"));
            }
        }
    }
    checkToggleTextFor(toggle) {
        let forAttr = toggle.getAttribute("toggle-id");
        let toggableElements = document.querySelectorAll('[toggle-for=' + forAttr + ']');
        let sameToggles = document.querySelectorAll('[toggle-id=' + forAttr + ']');

        // Check linked toggle fields for disabled status.
        for (let i = 0; i < toggableElements.length; i++) {
            this.handleToggleDisable(toggableElements[i], toggle.getAttribute("value") != toggableElements[i].getAttribute("toggle-value"));
        }

        // Check other toggles with the same toggle-id for consistent values.
        for (let i = 0; i < sameToggles.length; i++) {
            if (sameToggles[i].getAttribute("value") !== toggle.getAttribute("value")) {
                sameToggles[i].setAttribute("value", toggle.getAttribute("value"));
            }
        }
    }

    // Checks if toggle fields are disabled if necessary
    checkEditFor(toggle) {
        let forAttr = toggle.getAttribute("toggle-id");
        let toggableElements = document.querySelectorAll('[toggle-for=' + forAttr + ']');

        // Check linked toggle fields for disabled status.
        for (let i = 0; i < toggableElements.length; i++) {
            this.handleToggleDisable(toggableElements[i], toggle.getAttribute("edit") == "true");
        }
    }

    // Disable or enable toggle field based on "disabled" value
    handleToggleDisable(element, disabled) {
        if (disabled) {
            element.setAttribute("toggle-disabled", "");
        } else {
            element.removeAttribute("toggle-disabled");
        }
    }
}
