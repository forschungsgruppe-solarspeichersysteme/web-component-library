// The ViewHandler class manages the switching of main and sub views, as well as event listeners for buttons.
class TabHandler {
    // Declare instance variables.
    initializer;

    // NodeList of tab-button-panels.
    tabButtonPanels = {};

    tabClass = "tab-view";

    // Constructor to set up the ViewHandler.
    constructor(initializer) {
        this.initializer = initializer;
        this.registerTabButtonPanels();
        this.initTabButtonPanelsListener();
    }

    // Register tab views based on the specified class name.
    registerTabButtonPanels() {
        this.tabButtonPanels = document.querySelectorAll("tab-button-panel");
        for (let i = 0; i < this.tabButtonPanels.length; i++) {
            let tabView = document.querySelector("[id='" + this.tabButtonPanels[i].getAttribute("for") + "']");
            if(tabView == null){
                continue;
            }

            let individualViews = tabView.querySelectorAll(".tab")
            for (let j = 0; j < individualViews.length; j++) {
                if(this.tabButtonPanels[i].getAttribute("value") == j){
                    individualViews[j].classList.add("active");
                }else{
                    individualViews[j].classList.remove("active");
                }
            }                
        }
    }

    /**
     * Initialize event listeners for buttons in the main views.
     */
    initTabButtonPanelsListener() {

        // Iterate through buttons and set up event listeners for switching views.
        for (let i = 0; i < this.tabButtonPanels.length; i++) {
            this.tabButtonPanels[i].addEventListener("click", (e) => {
                let tabView = document.querySelector("[id='" + e.target.getAttribute("for") + "']");
                let individualViews = tabView.querySelectorAll(".tab")
                for (let i = 0; i < individualViews.length; i++) {
                    if(e.target.getAttribute("value") == i){
                        individualViews[i].classList.add("active");
                    }else{
                        individualViews[i].classList.remove("active");
                    }
                }   
                this.initializer.bridge.afterTabSwitch();             
            });
        }
    }

    /**
     * Initialize event listeners for subview navigation.
     */
    initSubViewsListener() {
        this.initializer.bridge.initSubViewsListener();
    }
}
