// The Bridge class acts as an intermediary between the Library and the tool itself
class Bridge extends AbstractBridge{
    links;

    constructor(initializer) {
        super(initializer);
    }

    finishedLoading(){
        if(UrlHelper.getGetParameter("subview")){
            this.initializer.viewHandler.switchSubViewById(UrlHelper.getGetParameter("subview"));
        }
    }
    // setMainLoading(value){

    // // Initialize event listeners for navigation buttons.
    // initListeners() {
    //     let that = this;
    //     this.links = document.querySelectorAll('a');
    //     this.links.forEach(function (link) {
    //         if(!link.hasAttribute('link-to')){
    //             return;
    //         }
    //         link.addEventListener('click', function(e) {
    //             e.preventDefault();
    //             UrlHelper.setGetParameter("subview", link.getAttribute('link-to'))
    //             that.initializer.viewHandler.switchSubViewById(link.getAttribute('link-to'));
    //         });            
    //     });
    // }   
    handleCustomEvent(value){
        UrlHelper.setGetParameter("subview", value)
        this.initializer.viewHandler.switchSubViewById(value);
    }
}
